export {
  HandleResponse,
  HandleStreamResponse,
  defaultHeader,
}

function HandleResponse(res) {
  if (!res.ok) throw res.status
  return res.json()
}

function HandleStreamResponse(res) {
  console.log(res)
  if (!res.ok) throw res.status
  return res.body
}

function defaultHeader() {
  // return authorization header with jwt token
  let user = JSON.parse(localStorage.getItem("user"));

  if (user && user.token) {
    return {
      "Authorization": "Bearer " + user.token,
      "Accept": "application/json",
      "Content-Type": "application/json",
      "mode": "cors",
    };
  } else {
    return {
      "Accept": "application/json",
      "Content-Type": "application/json",
      //"mode": "cors",
    }
  }
}