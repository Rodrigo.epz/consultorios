import Dashboard from "./Pages/Dashboard";
import Paciente from "./Pages/Paciente";
import Layout from "./Components/Layout/Layout";
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom";
import Calendario from "./Pages/Calendario";
import Respaldo from "./Pages/Respaldo";

function App() {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route exact path="/" component={Dashboard} />

          <Route
            exact
            path="/alta-pacientes"
            component={Paciente}
          />
          <Route
            path="/alta-pacientes/:PacienteId"
            component={Paciente}
          />
          <Route
            path="/calendario"
            component={Calendario}
          />
            <Route
            path="/respaldo"
            component={Respaldo}
          />
          <Route component={() => <p>not Found</p>}></Route>
        </Switch>
      </Layout>
    </Router>
  );
}

export default App;
