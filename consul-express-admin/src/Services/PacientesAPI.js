import { urlConstants } from "../Constants/UrlConstants";
import { defaultHeader, HandleResponse } from "../Helpers/HttpUtils";

export const PacientesApi = {
  Get,
  GetAll,
  GuardaPaciente,
  ActualizaPaciente,
  EliminaPaciente,
};

function Get(id) {
  const uri = "api/pacientes/";
  const requestOptions = {
    method: "GET",
    headers: defaultHeader(),
  };

  const queryParams = id ? `${id}` : "";

  return fetch(`${urlConstants.URLAPI}${uri}${queryParams}`, requestOptions)
    .then(HandleResponse)
    .then((response) => {
      return response;
    });
}

function GetAll(
  pagina = 0,
  itemsPorPagina = 0,
  ordenaPor,
  ordenaDireccion,
  searchParams = {},
  intentos = 255
) {
  const uri = "api/pacientes?";
  const requestOptions = {
    method: "GET",
    headers: defaultHeader(),
  };

  const queryParams =
    (pagina ? `pagina=${pagina}&` : "") +
    (itemsPorPagina ? `itemsPorPagina=${itemsPorPagina}&` : "") +
    (ordenaPor ? `ordenaPor=${ordenaPor}&` : "") +
    (ordenaDireccion ? `ordenaDireccion=${ordenaDireccion}&` : "") +
    (searchParams.general ? `general=${searchParams.general}&` : "") +
    (searchParams.nombre ? `nombre=${searchParams.nombre}&` : "") +
    (searchParams.apellidoPaterno ? `apellidoPaterno=${searchParams.apellidoPaterno}&` : "") +
    (searchParams.apellidoMaterno ? `apellidoMaterno=${searchParams.apellidoMaterno}&` : "") +
    (searchParams.motivo ? `motivo=${searchParams.motivo}` : "");

  return new Promise((resolve, reject) => {
    fetch(`${urlConstants.URLAPI}${uri}${queryParams}`, requestOptions)
      .then(HandleResponse)
      .then(resolve)
      .catch((error) => {
        console.log(error);
        if (intentos === 1 || error === 500) return reject(error);
        GetAll(pagina, itemsPorPagina, ordenaPor, ordenaDireccion, -1)
          .then(resolve)
          .catch(reject);
      });
  });
}

function GuardaPaciente(paciente) {
  const uri = "api/pacientes";
  paciente.id = "";
  paciente.edad = calculaEdad(paciente.fechaNacimiento);
    const data = JSON.stringify(paciente);
  const requestOptions = {
    method: "POST",
    headers: defaultHeader(),
    body: data,
  };

  return fetch(`${urlConstants.URLAPI}${uri}`, requestOptions)
    .then(HandleResponse)
    .then((response) => {
      return response;
    });
}

function ActualizaPaciente(paciente) {
  const uri = "api/pacientes";
  paciente.edad = calculaEdad(paciente.fechaNacimiento);
  const data = JSON.stringify(paciente);
  const requestOptions = {
    method: "PUT",
    headers: defaultHeader(),
    body: data,
  };

  return fetch(`${urlConstants.URLAPI}${uri}`, requestOptions)
    .then(HandleResponse)
    .then((response) => {
      return response;
    });
}

function EliminaPaciente(id) {
  const uri = "api/pacientes/";

  const requestOptions = {
    method: "DELETE",
    headers: defaultHeader(),
  };

  const queryParams = id ? `${id}` : "";

  return fetch(`${urlConstants.URLAPI}${uri}${queryParams}`, requestOptions)
    .then(HandleResponse)
    .then((response) => {
      return response;
    });
}

function calculaEdad(cumpleaños) {
  var diff_ms = new Date().setHours(0, 0, 0, 0) - cumpleaños.getTime();
  var edad = new Date(diff_ms);

  return Math.abs(edad.getUTCFullYear() - 1970);
}
