import { urlConstants } from "../Constants/UrlConstants";
import { blobHeader, defaultHeader, HandleResponse, HandleStreamResponse } from "../Helpers/HttpUtils";
import { saveAs } from 'file-saver';

export const DatosAPI = {
    GetHistory,
    CreateBackup
};

function GetHistory(intentos = 10) {
  const uri = "api/Datos/";
  const requestOptions = {
    method: "GET",
    headers: defaultHeader(),
  };

  return new Promise((resolve, reject) => {
    fetch(`${urlConstants.URLAPI}${uri}`, requestOptions)
      .then(HandleResponse)
      .then(resolve)
      .catch((error) => {
        console.log(error);
        if (intentos === 1 ) return reject(error);
        GetHistory()//Intentos Infinitos
          .then(resolve)
          .catch(reject);
      });
  });
}

function CreateBackup(){
  const uri = "api/Datos/save";
  const requestOptions = {
    method: "POST",
    responseType: "blob",
  };


  return fetch(`${urlConstants.URLAPI}${uri}`, requestOptions)
  .then(async response => { 
    console.log(response)
    // let filename = response.headers['content-disposition']
      // .split(';')
      // .find((n) => n.includes('filename='))
      // .replace('filename=', '')
      // .trim();  
    // console.log(filename)

       
    // let url = window.URL
    //   .createObjectURL();     
    let today = new Date()
    saveAs(await response.blob(), `pacientes_${today.getFullYear()}-${ padTo2Digits(today.getMonth() + 1)}-${padTo2Digits(today.getDate())}.xlsx`);    
});
}


function padTo2Digits(num) {
  return num.toString().padStart(2, '0');
}