import { urlConstants } from "../Constants/UrlConstants"
import { defaultHeader, HandleResponse } from "../Helpers/HttpUtils";

export const AntHeredoFamiliaresAPI = {
  GuardaAntecedente,
  ActualizaAntecedente,
}

function GuardaAntecedente(antecedente) {
  const uri = "api/antheredofamiliares"
  const data = JSON.stringify(antecedente)
  const requestOptions = {
    method: "POST",
    headers: defaultHeader(),
    body: data
  }

  return fetch(`${urlConstants.URLAPI}${uri}`, requestOptions).then(HandleResponse).then(response => { return response });
}

function ActualizaAntecedente(antecedente) {
  const uri = "api/antheredofamiliares"
  const data = JSON.stringify(antecedente)
  const requestOptions = {
    method: "PUT",
    headers: defaultHeader(),
    body: data
  }

  return fetch(`${urlConstants.URLAPI}${uri}`, requestOptions).then(HandleResponse).then(response => { return response });
}