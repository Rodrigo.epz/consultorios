import { urlConstants } from "../Constants/UrlConstants"
import { defaultHeader, HandleResponse } from "../Helpers/HttpUtils";

export const AntGinecoObstetricosAPI = {
  GuardaAntecedente,
  ActualizaAntecedente,
}

function GuardaAntecedente(antecedente) {
  const uri = "api/antginecoobstetricos"
  antecedente.abortos = parseInt(antecedente.abortos, 10)
  antecedente.cesareas = parseInt(antecedente.cesareas, 10)
  antecedente.gesta = parseInt(antecedente.gesta, 10)
  antecedente.ivsa = parseInt(antecedente.ivsa, 10)
  antecedente.monarca = parseInt(antecedente.monarca, 10)
  antecedente.para = parseInt(antecedente.para, 10)

  const data = JSON.stringify(antecedente)
  const requestOptions = {
    method: "POST",
    headers: defaultHeader(),
    body: data
  }

  return fetch(`${urlConstants.URLAPI}${uri}`, requestOptions).then(HandleResponse).then(response => { return response });
}

function ActualizaAntecedente(antecedente) {
  const uri = "api/antginecoobstetricos"
  antecedente.abortos = parseInt(antecedente.abortos, 10)
  antecedente.cesareas = parseInt(antecedente.cesareas, 10)
  antecedente.gesta = parseInt(antecedente.gesta, 10)
  antecedente.ivsa = parseInt(antecedente.ivsa, 10)
  antecedente.monarca = parseInt(antecedente.monarca, 10)
  antecedente.para = parseInt(antecedente.para, 10)

  const data = JSON.stringify(antecedente)
  const requestOptions = {
    method: "PUT",
    headers: defaultHeader(),
    body: data
  }

  return fetch(`${urlConstants.URLAPI}${uri}`, requestOptions).then(HandleResponse).then(response => { return response });
}