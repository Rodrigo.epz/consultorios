import { urlConstants } from "../Constants/UrlConstants";
import { defaultHeader, HandleResponse } from "../Helpers/HttpUtils";

export const HistorialAPI = {
  GuardaHistorial,
  ActualizaHistorial,
};

function GuardaHistorial(historial) {
  const uri = "api/historial";
  const data = JSON.stringify(historial);
  const requestOptions = {
    method: "POST",
    headers: defaultHeader(),
    body: data,
  };

  return fetch(`${urlConstants.URLAPI}${uri}`, requestOptions)
    .then(HandleResponse)
    .then((response) => {
      return response;
    });
}

function ActualizaHistorial(historial) {
  const uri = "api/historial";
  const data = JSON.stringify(historial);
  const requestOptions = {
    method: "PUT",
    headers: defaultHeader(),
    body: data,
  };

  return fetch(`${urlConstants.URLAPI}${uri}`, requestOptions)
    .then(HandleResponse)
    .then((response) => {
      return response;
    });
}
