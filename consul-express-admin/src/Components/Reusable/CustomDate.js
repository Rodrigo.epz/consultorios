import React, { useEffect, useState } from "react";
import DatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css";
import { Input } from "reactstrap";

export const CustomDate = (props) => {
    const [startDate, setStartDate] = useState(null);

    useEffect(()=>{
      console.log("calendar props", props)
      if(props.defDate){
        let values = props.defDate.split("/")
        setStartDate(new Date(values[2], values[1], values[0]))
      }
    },[props.defDate])

  return (
    <DatePicker className={props.errorStyle} 
    selected={startDate} 
    onChange={(date) => setStartDate(date)}
    dateFormat="dd/MM/yyyy"
     peekNextMonth
      showMonthDropdown
      showYearDropdown
      dropdownMode="select"
    customInput={<Input></Input>}/>
  );
  };

