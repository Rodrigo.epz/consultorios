import React, { Fragment, useEffect, useState } from "react";
import { Container } from "reactstrap";
import { Link, useLocation } from "react-router-dom";
import logo from "../../resources/Logo.png";

const Layout = (props) => {
  
  const [path, setPath] = useState("")
  const location = useLocation()

  useEffect(() => {
    console.log(location)
    setPath(location.pathname.substring(1));
  },[location.pathname])


  return (
    <Fragment>
      <div>
        <div className="consul-sidebar">
          <img src={logo} />
          <ul>
            <li className={path == "" ? "consul-active-menu" : ""}>
              <Link to="/">Inicio</Link>
            </li>
            <li className={path == "alta-pacientes" ? "consul-active-menu" : ""}>
              <Link to="/alta-pacientes">Alta de pacientes</Link>
            </li>
            <li className={path == "calendario" ? "consul-active-menu" : ""}>
              <Link to="/calendario">Calendario</Link>
            </li>
            <li className={path == "respaldo" ? "consul-active-menu" : ""}>
              <Link to="/respaldo">Respaldo</Link>
            </li>
            {/* <li className={path == "importar" ? "consul-active-menu" : ""}>
              <Link to="/importar">Importar</Link>
            </li> */}
          </ul>
        </div>
        <div className="consul-principal-content">
          <header>Gyne Clinic</header>

          <div className="consul-body">
            <Container fluid>{props.children}</Container>
          </div>

          <footer></footer>
        </div>
      </div>
    </Fragment>
  );
};

export default Layout;
