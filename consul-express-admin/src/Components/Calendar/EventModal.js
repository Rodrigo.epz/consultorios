import { useEffect } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Form,
  Row,
  Col,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { Controller, useForm } from "react-hook-form";

const EventModal = ({
  isOpen,
  evento,
  handleEventSubmit,
  closeModal,
  toogleWarning,
}) => {
  const defaultValues = {
    id: "",
    start: "",
    end: "",
    startHour: "",
    endHour: "",
    title: "",
  };

  const {
    handleSubmit,
    watch,
    control,
    formState: { errors },
    reset,
  } = useForm({ defaultValues });

  useEffect(() => {
    console.log("Evento: ", evento);
    if (evento && evento.start) { // Para bindear data al editar evento
      let eventoNuevo = defaultValues;
      eventoNuevo.id = evento.id;
      eventoNuevo.start = evento.startStr.split("T")[0];
      eventoNuevo.title = evento.title;
      eventoNuevo.startHour =
        ("0" + evento.start.getHours()).slice(-2) +
        ":" +
        ("0" + evento.start.getMinutes()).slice(-2);
      eventoNuevo.endHour =
        ("0" + evento.end.getHours()).slice(-2) +
        ":" +
        ("0" + evento.end.getMinutes()).slice(-2);


      reset({ ...eventoNuevo });
    }else if(evento && evento.dateStr){//Para bindear date al crear evento
      let eventoNuevo = defaultValues;
      eventoNuevo.start = evento.dateStr.split("T")[0];
      reset({ ...eventoNuevo });
    }
    if (Object.entries(evento).length === 0) {
      reset(defaultValues);
    }
  }, [evento, reset]);

  console.log("valores evento: ", defaultValues);
  return (
    <>
      <Modal isOpen={isOpen} toggle={closeModal}>
        <ModalHeader toggle={closeModal}>
          {Object.keys(evento).length ? "Editar " : "Crear "}Cita
        </ModalHeader>
        <Form onSubmit={handleSubmit(handleEventSubmit)}>
          <ModalBody>
            <Row>
              <Col md={12}>
                <FormGroup>
                  <Label for="title">Cita:</Label>
                  <Controller
                    control={control}
                    name="title"
                    rules={{ required: true }}
                    render={({ field }) => (
                      <Input
                        {...field}
                        className={errors.title && "consul-input-error"}
                      />
                    )}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col md={6}>
                <FormGroup>
                  <Label for="start">Fecha:</Label>
                  <Controller
                    control={control}
                    name="start"
                    rules={{ required: true }}
                    render={({ field }) => (
                      <Input
                        {...field}
                        type="date"
                        className={errors.start && "consul-input-error"}
                      />
                    )}
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label for="startHour">Hora de inicio:</Label>
                  <Controller
                    control={control}
                    name="startHour"
                    rules={{ required: true }}
                    render={({ field }) => (
                      <Input
                        {...field}
                        type="time"
                        className={errors.startHour && "consul-input-error"}
                      />
                    )}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col md={6}></Col>
              <Col md={6}>
                <FormGroup>
                  <Label for="endHour">Hora de final:</Label>
                  <Controller
                    control={control}
                    name="endHour"
                    rules={{ required: true }}
                    render={({ field }) => (
                      <Input
                        {...field}
                        type="time"
                        className={errors.endHour && "consul-input-error"}
                      />
                    )}
                  />
                </FormGroup>
              </Col>
            </Row>
          </ModalBody>
          <br />
          <ModalFooter>
            <Button color="primary" type="submit">
              Guardar
            </Button>
            <Button onClick={closeModal}>Cancelar</Button>
            {Object.keys(evento).length ? (
              <Button color="danger" onClick={toogleWarning}>
                Borrar
              </Button>
            ) : (
              <></>
            )}
          </ModalFooter>
        </Form>
      </Modal>
    </>
  );
};

export default EventModal;
