import React, { Fragment } from "react";
import { useTable, useBlockLayout } from "react-table";
import { useSticky } from "react-table-sticky";
import styled from "styled-components";

const Styles = styled.div`
  padding: 1rem;

  .table {
    border: 1px solid #ddd;

    .tr {
      :last-child {
        .td {
          border-bottom: 0;
        }
      }
    }

    .th,
    .td {
      padding: 5px;
      border-bottom: 1px solid #ddd;
      border-right: 1px solid #ddd;
      background-color: #fff;
      overflow: hidden;

      :last-child {
        border-right: 0;
      }

      .resizer {
        display: inline-block;
        width: 5px;
        height: 100%;
        position: absolute;
        right: 0;
        top: 0;
        transform: translateX(50%);
        z-index: 1;

        &.isResizing {
          background: red;
        }
      }
    }

    &.sticky {
      overflow: scroll;
      .header,
      .footer {
        position: sticky;
        z-index: 1;
        width: fit-content;
      }

      .header {
        top: 0;
        box-shadow: 0px 3px 3px #ccc;
      }

      .footer {
        bottom: 0;
        box-shadow: 0px -3px 3px #ccc;
      }

      .body {
        position: relative;
        z-index: 0;
      }

      [data-sticky-td] {
        position: sticky;
      }

      [data-sticky-last-left-td] {
        box-shadow: 2px 0px 3px #ccc;
      }

      [data-sticky-first-right-td] {
        box-shadow: -2px 0px 3px #ccc;
      }
    }
  }
`;




function Table({   columns ,
    data  ,
    fetchData,
    loading,
    pageCount: controlledPageCount,
    qtyRecords }) {
  const defaultColumn = React.useMemo(
    () => ({
      minWidth: 150,
    }),
    []
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state: { pageIndex, pageSize, sortBy, selectedRowIds },
  } = useTable(
    {
        columns,
      data,
      defaultColumn
    },
    useBlockLayout,
    useSticky
  );

  React.useEffect(() => {
    fetchData(0, 10, "")
  }, [sortBy, pageIndex, pageSize])

  // Workaround as react-table footerGroups doesn't provide the same internal data than headerGroups
  const footerGroups = headerGroups.slice().reverse();

  return (
    <Styles>
      <div
        {...getTableProps()}
        className="table sticky"
      >
        <div className="header">
          {headerGroups.map(headerGroup => (
            <div {...headerGroup.getHeaderGroupProps()} className="tr">
              {headerGroup.headers.map(column => (
                <div {...column.getHeaderProps()} className="th">
                  {column.render("Header")}
                </div>
              ))}
            </div>
          ))}
        </div>

        <div {...getTableBodyProps()} className="body">
          {rows.map((row, i) => {
            prepareRow(row);
            return (
              <div {...row.getRowProps()} className="tr">
                {row.cells.map(cell => {
                  return (
                    <div {...cell.getCellProps()} className="td">
                      {cell.render("Cell")}
                    </div>
                  );
                })}
              </div>
            );
          })}
        </div>

    
      </div>
    </Styles>
  );
}

function App({
    data,
    fetchData }) {

  const columns = [
    {
      Header: "Paciente",  
      columns: [
        {
          Header: "",
          accessor: "id",
        
          disableSortBy: true,
        },
        {
          Header: "Nombre",
          accessor: "nombre", // accessor is the "key" in the data
        },
        {
          Header: "Apellido P",
          accessor: "apellidoP",
        },
        {
          Header: "Apellido M",
          accessor: "apellidoM",
        },
        {
          Header: "Edad",
          accessor: "edad",
        },
        {
          Header: "Fecha Nacimiento",
          accessor: "fechaNacimiento",
          // Cell: (props) => {
          //   return (
          //     <div>
          //       {" "}
          //       {new Date(
          //         props.value.split("T")[0].split("-")
          //       ).toLocaleDateString()}{" "}
          //     </div>
          //   );
          // },
        },
        {
          Header: "Telefono",
          accessor: "telefono",
        },
        {
          Header: "Direccion",
          accessor: "direccion",
        },
        {
          Header: "Motivo Consulta",
          accessor: "motivoConsulta",
           Cell: (props) => {
            return (
              <div className="consul-dashboard-table-motivoConsul">
                {" "}
                {props.value}{" "}
              </div>
            );
          },
        },
      ],
    },
    {
      Header: "Informacion Medica",
      columns: [
        {
          Header: "Alcoholismo",
          accessor: "antPatologicos.alcoholismo",
          disableSortBy: true,
        },
        {
          Header: "Fumador",
          accessor: "antPatologicos.fumador",
          disableSortBy: true,
        },
        {
          Header: "Alergias",
          accessor: "antPatologicos.alergias",
          disableSortBy: true,
        },
      ],
    },
  ];


  return <Table columns={columns} data={data} fetchData={fetchData} />;
}

export default App;
