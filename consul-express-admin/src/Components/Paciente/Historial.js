import React, { useEffect } from "react";
import {
  Col,
  Row,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Container,
} from "reactstrap";
import { Controller, useForm } from "react-hook-form";
import swal from "sweetalert";
import { HistorialAPI } from "../../Services/HistorialAPI";

function Historial({ paciente, respaldaHistorial }) {
  const defaultValues = {
    subjetivo: "",
    laboratorioYGabinete: "",
    objetivo: "",
    analisisIDX: "",
    plan: "",
    id: 0,
  };

  const {
    handleSubmit,
    formState: { errors },
    reset,
    control,
  } = useForm({ defaultValues });

  useEffect(() => {
    console.log("desde historial ", paciente);

    if (paciente && paciente.historial) {
      console.log("defaulValues historial: ", paciente.historial);
      reset({ ...paciente.historial });
    }
    if (Object.entries(paciente).length === 0) {
      reset(defaultValues);
    }
  }, [paciente.historial]);

  const onSubmit = (data) => {
    data.pacienteId = paciente.id;
    console.log("Enviando: ", data);

    if (paciente.historial && paciente.historial.id) {
      HistorialAPI.ActualizaHistorial(data)
        .then((res) => {
          respaldaHistorial(formatHistorialObject(res));
          swal({
            title: "Bien!",
            text: "Se ha actualizado el historial",
            icon: "success",
            buttons: {
              confirm: { text: "Ok", className: "btn-primary" },
            },
          });
        })
        .catch((err) => {
          console.log("Error: ", err);
          swal({
            title: "Error!",
            text: "Hubo un error intentando actualizar el historial",
            icon: "error",
            confirmButtonColor: "#FFF",
          });
        });
    } else {
      HistorialAPI.GuardaHistorial(data)
        .then((res) => {
          respaldaHistorial(formatHistorialObject(res));
          swal({
            title: "Bien!",
            text: "Se ha creado el Historial",
            icon: "success",
            buttons: {
              confirm: { text: "Ok", className: "btn-primary" },
            },
          });
        })
        .catch((err) => {
          console.log("Error: ", err);
          swal({
            title: "Error!",
            text: "Hubo un error intentando crear el Historial",
            icon: "error",
            confirmButtonColor: "#FFF",
          });
        });
    }
  };

  const formatHistorialObject = (Historial) => {
    return {
      contenido: {
        ...Historial,
      },
      nombre: "historial",
    };
  };

  return (
    <Container>
      <div className="consul-titulo-generico">
        <h5>Historial</h5>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)} className="consul-historial-form">
        <Row>
          <Col md={6}>
            <FormGroup>
              <Label for="subjetivo">Subjetivo:</Label>
              <Controller
                control={control}
                name="subjetivo"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    className={errors.subjetivo && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="laboratorioYGabinete">Laboratorio y gabinete:</Label>
              <Controller
                control={control}
                name="laboratorioYGabinete"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    className={
                      errors.laboratorioYGabinete && "consul-input-error"
                    }
                  />
                )}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <FormGroup>
              <Label for="objetivo">Objetivo:</Label>
              <Controller
                control={control}
                name="objetivo"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    className={errors.objetivo && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="analisisIDX">Analisis (I. DX):</Label>
              <Controller
                control={control}
                name="analisisIDX"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    className={errors.analisisIDX && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <FormGroup>
              <Label for="plan">Plan:</Label>
              <Controller
                control={control}
                name="plan"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    maxLength="10000"
                    className={errors.plan && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={8}></Col>
          <Col md={4}>
            <Button
              type="submit"
              color="primary"
              className="float-end"
              disabled={paciente.id ? false : true}
            >
              Guardar
            </Button>
          </Col>
        </Row>
      </Form>
    </Container>
  );
}

export default Historial;
