import React, { forwardRef, useEffect } from "react";
import {
  Col,
  Row,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Container,
} from "reactstrap";
import { Controller, useForm } from "react-hook-form";
import NumberFormat from "react-number-format";
import { PacientesApi } from "../../Services/PacientesAPI";
import { CustomDate } from "../Reusable/CustomDate";
import swal from "sweetalert";
import ReactDatePicker from "react-datepicker";

function InfoGeneral({ paciente, respaldaPaciente }) {
  const defaultValues = {
    apellidoM: "",
    apellidoP: "",
    direccion: "",
    edad: 0,
    fechaNacimiento: "",
    id: "",
    motivoConsulta: "",
    nombre: "",
    telefono: "",
  };

  const ExampleCustomInput = forwardRef(({ value, onClick }, ref) => (
      <button
        ref={ref}
        className={errors.fechaNacimiento && "consul-input-error"}
      />
  ));

  const {
    handleSubmit,
    watch,
    control,
    formState: { errors },
    reset,
  } = useForm({ defaultValues });

  useEffect(() => {
    console.log("defaultValues infoGeneral: ", paciente);
    if (paciente && paciente.id) {
      let pacienteNuevo = paciente;
      if(typeof pacienteNuevo.fechaNacimiento == "string"){
        let values = pacienteNuevo.fechaNacimiento && pacienteNuevo.fechaNacimiento.split("T")[0].split("/");
        pacienteNuevo.fechaNacimiento = new Date(values[2], values[1] - 1, values[0])
      }
      reset({ ...pacienteNuevo });
    }
    if (Object.entries(paciente).length === 0) {
      reset(defaultValues);
    }
  }, [paciente, reset]);

  const onSubmit = (data) => {
    console.log("Enviando: ", data);
    if (paciente && paciente.id) {
      PacientesApi.ActualizaPaciente(data)
        .then((res) => {
          respaldaPaciente(res);
          swal({
            title: "Bien!",
            text: "Se ah editado la informacion general del paciente",
            icon: "success",
            buttons: {
              confirm: { text: "Ok", className: "btn-primary" },
            },
          });
        })
        .catch((err) => {
          console.log("Error: ", err);
          swal({
            title: "Error!",
            text: "Hubo un error intentando editar el paciente",
            icon: "error",
            confirmButtonColor: "#FFF",
          });
        });
    } else {
      PacientesApi.GuardaPaciente(data)
        .then((res) => {
          respaldaPaciente(res);
          swal({
            title: "Bien!",
            text: "Se ah creado un paciente nuevo",
            icon: "success",
            buttons: {
              confirm: { text: "Ok", className: "btn-primary" },
            },
          });
        })
        .catch((err) => {
          console.log("Error: ", err);
          if (err === 409) {
            swal({
              text: "el paciente ya existe",
              icon: "error",
              buttons: {
                confirm: { text: "Ok", className: "btn-primary" },
              },
            });
          } else {
            swal({
              title: "Error!",
              text: "Hubo un error intentando crear el paciente",
              icon: "error",
              confirmButtonColor: "#FFF",
            });
          }
        });
    }
  };

  return (
    <Container>
      
      <div className="consul-titulo-generico">
        <h5>Informacion general del paciente</h5>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Row>
          <Col md={4}>
            <FormGroup>
              <Label for="nombre">Nombre(s):</Label>
              <Controller
                control={control}
                name="nombre"
                rules={{ required: true }}
                render={({ field }) => (
                  <Input
                    {...field}
                    className={errors.nombre && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="apellidoP">Apellido Paterno:</Label>
              <Controller
                control={control}
                name="apellidoP"
                rules={{ required: true }}
                render={({ field }) => (
                  <Input
                    {...field}
                    className={errors.apellidoP && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="apellidoM">Apellido Materno:</Label>
              <Controller
                control={control}
                name="apellidoM"
                render={({ field }) => (
                  <Input
                    {...field}
                    className={errors.apellidoM && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={5}>
            <FormGroup>
              <Label for="direccion">Direccion:</Label>
              <Controller
                control={control}
                name="direccion"
                rules={{ required: true }}
                render={({ field }) => (
                  <Input
                    {...field}
                    className={errors.direccion && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={3}>
            <FormGroup>
              <Label for="fechaNacimiento">Fecha de nacimiento:</Label>
              <Controller
                control={control}
                name="fechaNacimiento"
                rules={{ required: true }}
                render={({ field }) => (
                  <ReactDatePicker
                  //{...field}
                  selected = {field.value}
                  placeholderText='Select date'
                  onChange={(date) => field.onChange(date)}
                  customInput={<Input></Input>}
                  dateFormat="dd/MM/yyyy"
                  peekNextMonth
                  showMonthDropdown
                  showYearDropdown
                  dropdownMode="select"
                  errorStyle={errors.fechaNacimiento && "consul-input-error"}
                  />
                
                )}
              />
            </FormGroup>
          </Col>
          <Col md={2}>
            <FormGroup>
              <Label for="telefono">Telefono:</Label>
              <Controller
                control={control}
                name="telefono"
                rules={{ pattern: /[0-9]{2}-[0-9]{4}-[0-9]{4}/ }}
                render={({ field }) => (
                  <NumberFormat
                    {...field}
                    customInput={Input}
                    format="##-####-####"
                    className={errors.telefono && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={2}>
            <FormGroup className="consul-chechbox-container">
              <Label for="lotus">Sist. Lotus:</Label>
              <Controller
                control={control}
                name="lotus"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="checkbox"
                    className={errors.dismenorreica && "consul-input-error"}
                    checked={watch("lotus")}
                  />
                )}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <FormGroup>
              <Label for="motivoConsulta">Motivo de consulta:</Label>
              <Controller
                control={control}
                name="motivoConsulta"
                rules={{ required: true, maxLength: 1000 }}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    className={errors.motivoConsulta && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={8}></Col>
          <Col md={4}>
            <Button type="submit" color="primary" className="float-end">
              Guardar
            </Button>
          </Col>
        </Row>
      </Form>
    </Container>
  );
}

export default InfoGeneral;
