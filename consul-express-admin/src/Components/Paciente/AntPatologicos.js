import React, { useEffect } from "react"
import { Col, Row, Button, Form, FormGroup, Label, Input, Container } from "reactstrap"
import { Controller, useForm } from "react-hook-form"
import swal from "sweetalert"
import { AntPatologicosApi } from "../../Services/AntPatologicosAPI"


function AntPatologicos({ paciente, respaldaAntecedente }) {
  const defaultValues = {
    alcoholismo: "",
    alergias: "",
    cirugias: "",
    enfermedades: "",
    fracturas: "",
    fumador: "",
    grupoRH: "",
    id: 0,
    transfusiones: ""
  }

  const { handleSubmit, formState: { errors }, reset, control } = useForm({ defaultValues })

  useEffect(() => {
    if (paciente && paciente.antPatologicos) {
      console.log("defaulValues antPatologicos: ", paciente.antPatologicos)
      reset({ ...paciente.antPatologicos })
    }
    if (Object.entries(paciente).length === 0) {
      reset(defaultValues)
    }
  }, [paciente.antPatologicos])

  const onSubmit = (data) => {
    data.pacienteId = paciente.id
    console.log("Enviando: ", data);

    if (paciente.antPatologicos && paciente.antPatologicos.id) {
      AntPatologicosApi.ActualizaAntecedente(data)
        .then(res => {
          respaldaAntecedente(formatAntecedenteObject(res))
          swal({
            title: "Bien!",
            text: "Se han actualizado los antecedentes",
            icon: "success",
            buttons: {
              confirm: { text: "Ok", className: "btn-primary" },
            },
          });
        })
        .catch(err => {
          console.log("Error: ", err)
          swal({
            title: "Error!",
            text: "Hubo un error intentando actualizar el antecedente",
            icon: "error",
            confirmButtonColor: "#FFF"
          });
        })
    } else {
      AntPatologicosApi.GuardaAntecedente(data)
        .then(res => {
          respaldaAntecedente(formatAntecedenteObject(res))
          swal({
            title: "Bien!",
            text: "Se han creado los antecedentes",
            icon: "success",
            buttons: {
              confirm: { text: "Ok", className: "btn-primary" },
            },
          });
        })
        .catch(err => {
          console.log("Error: ", err)
          swal({
            title: "Error!",
            text: "Hubo un error intentando crear el antecedente",
            icon: "error",
            confirmButtonColor: "#FFF"
          });

        })
    }
  }

  const formatAntecedenteObject = (antecedente) => {
    return {
      contenido: {
        ...antecedente
      },
      nombre: "antPatologicos"
    }
  }

  return (
    <Container>
      <div className="consul-titulo-generico">
        <h5>Antecedentes Patologicos</h5>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Row>
          <Col md={4}>
            <FormGroup>
              <Label for="fumador">Fumador:</Label>
              <Controller
                control={control}
                name="fumador"
                rules={{ maxLength: 1000 }}
                render={({ field }) => (
                  <Input
                    {...field}
                    maxLength="1000"
                    className={(errors.fumador && "consul-input-error")}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="alcoholismo">Alcoholismo:</Label>
              <Controller
                control={control}
                name="alcoholismo"
                rules={{ maxLength: 15 }}
                render={({ field }) => (
                  <Input
                    {...field}
                    maxLength="15"
                    className={(errors.alcoholismo && "consul-input-error")}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="grupoRH">Grupo RH:</Label>
              <Controller
                control={control}
                name="grupoRH"
                rules={{ maxLength: 15 }}
                render={({ field }) => (
                  <Input
                    {...field}
                    maxLength="15"
                    className={(errors.grupoRH && "consul-input-error")}
                  />
                )}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <FormGroup>
              <Label for="alergias">Alergias:</Label>
              <Controller
                control={control}
                name="alergias"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    className={(errors.alergias && "consul-input-error")}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="fracturas">Fracturas:</Label>
              <Controller
                control={control}
                name="fracturas"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    className={(errors.fracturas && "consul-input-error")}
                  />
                )}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <FormGroup>
              <Label for="transfusiones">Transfusiones:</Label>
              <Controller
                control={control}
                name="transfusiones"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    className={(errors.transfusiones && "consul-input-error")}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="cirugias">Cirugias:</Label>
              <Controller
                control={control}
                name="cirugias"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    className={(errors.cirugias && "consul-input-error")}
                  />
                )}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <FormGroup>
              <Label for="enfermedades">Enfermedades:</Label>
              <Controller
                control={control}
                name="enfermedades"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    className={(errors.enfermedades && "consul-input-error")}
                  />
                )}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={8}>
          </Col>
          <Col md={4} >
            <Button type="submit" color="primary" className="float-end" disabled={paciente.id ? false : true}>Guardar</Button>
          </Col>
        </Row>
      </Form>
    </Container>
  )
}

export default AntPatologicos