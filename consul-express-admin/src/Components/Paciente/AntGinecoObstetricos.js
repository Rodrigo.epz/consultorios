import React, { useEffect } from "react";
import {
  Col,
  Row,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Container,
} from "reactstrap";
import { Controller, useForm } from "react-hook-form";
import swal from "sweetalert";
import { AntGinecoObstetricosAPI } from "../../Services/AntGinecoObstetricosAPI";

function AntGinecoObstetricos({ paciente, respaldaAntecedente }) {
  const defaultValues = {
    abortos: 0,
    cesareas: 0,
    dismenorreica: false,
    doc: "",
    eumenorreica: false,
    fechaPrimerParto: "",
    fpp: "",
    fua: "",
    fuc: "",
    fum: "",
    fup: "",
    gesta: 0,
    id: 0,
    ivsa: 0,
    monarca: 0,
    mpf: "",
    para: 0,
    ritmo: "",
  };
  const {
    handleSubmit,
    watch,
    formState: { errors },
    reset,
    control,
  } = useForm({ defaultValues });

  useEffect(() => {
    if (paciente && paciente.antGinecoObstetricos) {
      console.log(
        "defaulValues antGinecoObstetricos: ",
        paciente.antGinecoObstetricos
      );
      reset({ ...paciente.antGinecoObstetricos });
    }
    if (Object.entries(paciente).length === 0) {
      reset(defaultValues);
    }
  }, [paciente.antGinecoObstetricos]);

  const onSubmit = (data) => {
    data.pacienteId = paciente.id;
    console.log("Enviando: ", data);

    if (paciente.antGinecoObstetricos && paciente.antGinecoObstetricos.id) {
      AntGinecoObstetricosAPI.ActualizaAntecedente(data)
        .then((res) => {
          respaldaAntecedente(formatAntecedenteObject(res));
          swal({
            title: "Bien!",
            text: "Se han actualizado los antecedentes",
            icon: "success",
            buttons: {
              confirm: { text: "Ok", className: "btn-primary" },
            },
          });
        })
        .catch((err) => {
          console.log("Error: ", err);
          swal({
            title: "Error!",
            text: "Hubo un error intentando actualizar el antecedente",
            icon: "error",
            confirmButtonColor: "#FFF",
          });
        });
    } else {
      AntGinecoObstetricosAPI.GuardaAntecedente(data)
        .then((res) => {
          respaldaAntecedente(formatAntecedenteObject(res));
          swal({
            title: "Bien!",
            text: "Se han creado los antecedentes",
            icon: "success",
            buttons: {
              confirm: { text: "Ok", className: "btn-primary" },
            },
          });
        })
        .catch((err) => {
          console.log("Error: ", err);
          swal({
            title: "Error!",
            text: "Hubo un error intentando crear el antecedente",
            icon: "error",
            confirmButtonColor: "#FFF",
          });
        });
    }
  };

  const formatAntecedenteObject = (antecedente) => {
    return {
      contenido: {
        ...antecedente,
      },
      nombre: "antGinecoObstetricos",
    };
  };

  return (
    <Container>
      <div className="consul-titulo-generico">
        <h5>Antecedentes Patologicos</h5>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Row>
          <Col md={4}>
            <FormGroup>
              <Label for="monarca">Menarca:</Label>
              <Controller
                control={control}
                name="monarca"
                rules={{ required: true }}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    min="0"
                    className={errors.monarca && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="ritmo">Ritmo:</Label>
              <Controller
                control={control}
                name="ritmo"
                rules={{ maxLength: 15 }}
                render={({ field }) => (
                  <Input
                    {...field}
                    maxLength="15"
                    className={errors.ritmo && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup className="consul-chechbox-container">
              <Label for="eumenorreica">Eumenorreica:</Label>
              <Controller
                control={control}
                name="eumenorreica"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="checkbox"
                    className={errors.eumenorreica && "consul-input-error"}
                    checked={watch("eumenorreica")}
                  />
                )}
              />
            </FormGroup>
            <FormGroup className="consul-chechbox-container">
              <Label for="dismenorreica">Dismenorreica:</Label>
              <Controller
                control={control}
                name="dismenorreica"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="checkbox"
                    className={errors.dismenorreica && "consul-input-error"}
                    checked={watch("dismenorreica")}
                  />
                )}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={2}>
            <FormGroup>
              <Label for="ivsa">IVSA:</Label>
              <Controller
                control={control}
                name="ivsa"
                rules={{ required: true }}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    min="0"
                    className={errors.ivsa && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={2}>
            <FormGroup>
              <Label for="gesta">Gesta:</Label>
              <Controller
                control={control}
                name="gesta"
                rules={{ required: true }}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    min="0"
                    className={errors.gesta && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={2}>
            <FormGroup>
              <Label for="para">Para:</Label>
              <Controller
                control={control}
                name="para"
                rules={{ required: true }}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    min="0"
                    className={errors.para && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={2}>
            <FormGroup>
              <Label for="abortos">Abortos:</Label>
              <Controller
                control={control}
                name="abortos"
                rules={{ required: true }}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    min="0"
                    className={errors.abortos && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={2}>
            <FormGroup>
              <Label for="cesareas">Cesareas:</Label>
              <Controller
                control={control}
                name="cesareas"
                rules={{ required: true }}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    min="0"
                    className={errors.cesareas && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={4}>
            <FormGroup>
              <Label for="fup">Fup:</Label>
              <Controller
                control={control}
                name="fup"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    className={errors.fup && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="fuc">Fuc:</Label>
              <Controller
                control={control}
                name="fuc"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    className={errors.fuc && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="fua">Fua:</Label>
              <Controller
                control={control}
                name="fua"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    className={errors.fua && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <FormGroup>
              <Label for="fechaPrimerParto">Fecha de primer parto:</Label>
              <Controller
                control={control}
                name="fechaPrimerParto"
                render={({ field }) => (
                  <Input
                    {...field}
                    className={errors.fechaPrimerParto && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="mpf">Mpf:</Label>
              <Controller
                control={control}
                name="mpf"
                rules={{ maxLength: 50 }}
                render={({ field }) => (
                  <Input
                    {...field}
                    maxLength="50"
                    className={errors.mpf && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={4}>
            <FormGroup>
              <Label for="doc">Doc:</Label>
              <Controller
                control={control}
                name="doc"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    className={errors.doc && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="fum">Fum:</Label>
              <Controller
                control={control}
                name="fum"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    className={errors.fum && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="fpp">Fpp:</Label>
              <Controller
                control={control}
                name="fpp"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    className={errors.fpp && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={8}></Col>
          <Col md={4}>
            <Button
              type="submit"
              color="primary"
              className="float-end"
              disabled={paciente.id ? false : true}
            >
              Guardar
            </Button>
          </Col>
        </Row>
      </Form>
    </Container>
  );
}

export default AntGinecoObstetricos;
