import React, { useEffect } from "react";
import {
  Col,
  Row,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Container,
} from "reactstrap";
import { Controller, useForm } from "react-hook-form";
import swal from "sweetalert";
import { AntHeredoFamiliaresAPI } from "../../Services/AntHeredoFamiliaresAPI";

function AntHeredoFamiliares({ paciente, respaldaAntecedente }) {
  const defaultValues = {
    asma: "",
    cancer: "",
    enfCronicoDegenerativas: "",
    id: 0,
    tuberculosis: "",
  };
  const {
    handleSubmit,
    formState: { errors },
    reset,
    control,
  } = useForm({ defaultValues });

  useEffect(() => {
    if (paciente && paciente.antHeredoFamiliares) {
      console.log(
        "defaulValues antHeredoFamiliares: ",
        paciente.antHeredoFamiliares
      );
      reset({ ...paciente.antHeredoFamiliares });
    }
    if (Object.entries(paciente).length === 0) {
      reset(defaultValues);
    }
  }, [paciente.antHeredoFamiliares]);

  const onSubmit = (data) => {
    data.pacienteId = paciente.id;
    console.log("Enviando: ", data);

    if (paciente.antHeredoFamiliares && paciente.antHeredoFamiliares.id) {
      AntHeredoFamiliaresAPI.ActualizaAntecedente(data)
        .then((res) => {
          respaldaAntecedente(formatAntecedenteObject(res));
          swal({
            title: "Bien!",
            text: "Se han actualizado los antecedentes",
            icon: "success",
            buttons: {
              confirm: { text: "Ok", className: "btn-primary" },
            },
          });
        })
        .catch((err) => {
          console.log("Error: ", err);
          swal({
            title: "Error!",
            text: "Hubo un error intentando actualizar el antecedente",
            icon: "error",
            confirmButtonColor: "#FFF",
          });
        });
    } else {
      AntHeredoFamiliaresAPI.GuardaAntecedente(data)
        .then((res) => {
          respaldaAntecedente(formatAntecedenteObject(res));
          swal({
            title: "Bien!",
            text: "Se han creado los antecedentes",
            icon: "success",
            buttons: {
              confirm: { text: "Ok", className: "btn-primary" },
            },
          });
        })
        .catch((err) => {
          console.log("Error: ", err);
          swal({
            title: "Error!",
            text: "Hubo un error intentando crear el antecedente",
            icon: "error",
            confirmButtonColor: "#FFF",
          });
        });
    }
  };

  const formatAntecedenteObject = (antecedente) => {
    return {
      contenido: {
        ...antecedente,
      },
      nombre: "antHeredoFamiliares",
    };
  };

  return (
    <Container>
      <div className="consul-titulo-generico">
        <h5>Antecedentes Patologicos</h5>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Row>
          <Col md={4}>
            <FormGroup>
              <Label for="cancer">Cancer:</Label>
              <Controller
                control={control}
                name="cancer"
                render={({ field }) => (
                  <Input
                    {...field}
                    maxLength="20"
                    className={errors.cancer && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="tuberculosis">Tuberculosis:</Label>
              <Controller
                control={control}
                name="tuberculosis"
                rules={{ maxLength: 30 }}
                render={({ field }) => (
                  <Input
                    {...field}
                    maxLength="30"
                    className={errors.tuberculosis && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="asma">Asma:</Label>
              <Controller
                control={control}
                name="asma"
                rules={{ maxLength: 30 }}
                render={({ field }) => (
                  <Input
                    {...field}
                    maxLength="30"
                    className={errors.asma && "consul-input-error"}
                  />
                )}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <FormGroup>
              <Label for="enfCronicoDegenerativas">
                Enfermedades cronico degenerativas:
              </Label>
              <Controller
                control={control}
                name="enfCronicoDegenerativas"
                render={({ field }) => (
                  <Input
                    {...field}
                    type="textarea"
                    maxLength="1000"
                    className={
                      errors.enfCronicoDegenerativas && "consul-input-error"
                    }
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={6}></Col>
        </Row>

        <Row>
          <Col md={8}></Col>
          <Col md={4}>
            <Button
              type="submit"
              color="primary"
              className="float-end"
              disabled={paciente.id ? false : true}
            >
              Guardar
            </Button>
          </Col>
        </Row>
      </Form>
    </Container>
  );
}

export default AntHeredoFamiliares;
