import React, { useEffect } from "react"
import { Col, Row, Button, Form, FormGroup, Label, Input, Container } from "reactstrap"
import { Controller, useForm } from "react-hook-form"
import swal from "sweetalert"
import { AntNoPatologicosAPI } from "../../Services/AntNoPatologicosAPI"


function AntNoPatologicos({ paciente, respaldaAntecedente }) {
  const defaultValues = {
    estadoCivil: "",
    habitosHigieneDieta: "",
    id: 0,
    ocupacion: ""
  }
  const { handleSubmit, formState: { errors }, reset, control } = useForm({ defaultValues })

  useEffect(() => {

    if (paciente && paciente.antNOPatologicos) {
      console.log("defaulValues antNOPatologicos: ", paciente.antNOPatologicos)
      reset({ ...paciente.antNOPatologicos })
    }
    if (Object.entries(paciente).length === 0) {
      reset(defaultValues)
    }
  }, [paciente.antNOPatologicos])

  const onSubmit = (data) => {
    data.pacienteId = paciente.id
    console.log("Enviando: ", data);

    if (paciente.antNOPatologicos && paciente.antNOPatologicos.id) {
      AntNoPatologicosAPI.ActualizaAntecedente(data)
        .then(res => {
          respaldaAntecedente(formatAntecedenteObject(res))
          swal({
            title: "Bien!",
            text: "Se han actualizado los antecedentes",
            icon: "success",
            buttons: {
              confirm: { text: "Ok", className: "btn-primary" },
            },
          });
        })
        .catch(err => {
          console.log("Error: ", err)
          swal({
            title: "Error!",
            text: "Hubo un error intentando actualizar el antecedente",
            icon: "error",
            confirmButtonColor: "#FFF"
          });
        })
    } else {
      AntNoPatologicosAPI.GuardaAntecedente(data)
        .then(res => {
          respaldaAntecedente(formatAntecedenteObject(res))
          swal({
            title: "Bien!",
            text: "Se han creado los antecedentes",
            icon: "success",
            buttons: {
              confirm: { text: "Ok", className: "btn-primary" },
            },
          });
        })
        .catch(err => {
          console.log("Error: ", err)
          swal({
            title: "Error!",
            text: "Hubo un error intentando crear el antecedente",
            icon: "error",
            confirmButtonColor: "#FFF"
          });

        })
    }
  }

  const formatAntecedenteObject = (antecedente) => {
    return {
      contenido: {
        ...antecedente
      },
      nombre: "antNOPatologicos"
    }
  }

  return (
    <Container>
      <div className="consul-titulo-generico">
        <h5>Antecedentes Patologicos</h5>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Row>
          <Col md={4}>
            <FormGroup>
              <Label for="estadoCivil">Estado Civil:</Label>
              <Controller
                control={control}
                name="estadoCivil"
                rules={{ maxLength: 20 }}
                render={({ field }) => (
                  <Input
                    {...field}
                    maxLength="20"
                    className={(errors.estadoCivil && "consul-input-error")}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="ocupacion">Ocupacion:</Label>
              <Controller
                control={control}
                name="ocupacion"
                rules={{ maxLength: 20 }}
                render={({ field }) => (
                  <Input
                    {...field}
                    maxLength="20"
                    className={(errors.ocupacion && "consul-input-error")}
                  />
                )}
              />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="habitosHigieneDieta">Habitos de higiene y dieta:</Label>
              <Controller
                control={control}
                name="habitosHigieneDieta"
                rules={{ maxLength: 15 }}
                render={({ field }) => (
                  <Input
                    {...field}
                    maxLength="15"
                    className={(errors.habitosHigieneDieta && "consul-input-error")}
                  />
                )}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={8}>
          </Col>
          <Col md={4} >
            <Button type="submit" color="primary" className="float-end" disabled={paciente.id ? false : true}>Guardar</Button>
          </Col>
        </Row>
      </Form>
    </Container>
  )
}

export default AntNoPatologicos