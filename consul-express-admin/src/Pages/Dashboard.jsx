import React, { Fragment, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { PacientesApi } from "../Services/PacientesAPI";
import CustomTable from "../Components/Dashboard/Table";
import CustomTable1 from "../Components/Dashboard/TableExp";
import { columns } from "../Constants/tablaPacientesConstants";
import { Button, Col, Input, Label, Row, Form, Container } from "reactstrap";

const Dashboard = () => {
  const defaultValues = {
    general: "",
    nombre: "",
    apellidoPaterno: "",
    apellidoMaterno: "",
    motivo: "",
  };

  const {
    handleSubmit,
    watch,
    control,
    formState: { errors },
    reset,
  } = useForm({ defaultValues });

  const [pacientes, setPacientes] = useState([]);
  const [cantidadPacientes, setCantidadPacientes] = useState(true);
  const [cantidadPaginas, setCantidadPaginas] = useState(true);
  const [loading, setLoading] = useState(true);

  const getPacientes = (
    pageIndex = "0",
    pageSize = "10",
    sort = {},
    search = {}
  ) => {
    console.log(
      "obteniendo pacientes con la sig info:",
      pageIndex,
      pageSize,
      sort,
      search
    );

    const sortBy = sort.id ? sort.id : "";
    const sortDirection = sort.id && sort.desc ? "DESC" : "ASC";

    setLoading(true);
    PacientesApi.GetAll(
      pageIndex,
      pageSize,
      sortBy,
      sortDirection,
      search
    ).then((res) => {
      setPacientes(res.pacientes);
      setCantidadPacientes(res.cantidadPacientes);
      setCantidadPaginas(res.cantidadPaginas);
      setLoading(false);
    });
  };

  const onSearch = (data) => {
    getPacientes("0", "10", {}, data);
  };

  const handleReset = () => {
    reset(defaultValues);
  };

  return (
    <Fragment>
      <Container >
      <Form onSubmit={handleSubmit(onSearch)}>
        <Row>
          <Col md={12}>
            <Label for="general">General:</Label>
            <Controller
              control={control}
              name="general"
              render={({ field }) => (
                <Input
                  {...field}
                  className={errors.general && "consul-input-error"}
                />
              )}
            />
          </Col>
        </Row>
        <Row>
          <Col md={4}>
            <Label for="nombre">Nombre:</Label>
            <Controller
              control={control}
              name="nombre"
              render={({ field }) => (
                <Input
                  {...field}
                  className={errors.nombre && "consul-input-error"}
                  disabled={watch("general")}
                />
              )}
            />
          </Col>
          <Col md={4}>
            <Label for="apellido">Apellido Paterno:</Label>
            <Controller
              control={control}
              name="apellidoPaterno"
              render={({ field }) => (
                <Input
                  {...field}
                  className={errors.apellidoPaterno && "consul-input-error"}
                  disabled={watch("general")}
                />
              )}
            />
          </Col>
          <Col md={4}>
            <Label for="apellido">Apellido Materno:</Label>
            <Controller
              control={control}
              name="apellidoMaterno"
              render={({ field }) => (
                <Input
                  {...field}
                  className={errors.apellidoMaterno && "consul-input-error"}
                  disabled={watch("general")}
                />
              )}
            />
          </Col>
        </Row>
        <Row>
        <Col md={12}>
            <Label for="motivo">Motivo:</Label>
            <Controller
              control={control}
              name="motivo"
              render={({ field }) => (
                <Input
                  {...field}
                  className={errors.motivo && "consul-input-error"}
                  disabled={watch("general")}
                />
              )}
            />
          </Col>
        </Row>
        <Row>
          <Col md={8}></Col>
          <Col md={2}>
            <Button type="submit" className="btn-primary">
              Buscar
            </Button>
          </Col>
          <Col md={2}>
            <Button onClick={handleReset} className="btn-secundary">
              Limpiar
            </Button>
          </Col>
        </Row>
      </Form>
      </Container>

      <Container >
        <CustomTable
          columns={columns}
          data={pacientes}
          loading={loading}
          fetchData={getPacientes}
          pageCount={cantidadPaginas}
          qtyRecords={cantidadPacientes}
        />
      </Container>
    </Fragment>
  );
};

export default Dashboard;
