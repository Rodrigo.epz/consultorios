import React, { Fragment, useEffect, useState } from "react";
import { get, useForm } from "react-hook-form";
import { DatosAPI } from "../Services/DatosAPI";
import CustomTable from "../Components/Dashboard/Table";
import { columns } from "../Constants/tablaPacientesConstants";
import { Button, Col, Input, Label, Row, Form, Table, Spinner, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { PacientesApi } from "../Services/PacientesAPI";

const Respaldo = () => {

  useEffect(() => {
    getHistoryAndNumPacientes()
  },[])

  const [cantidadPAcientes, setCantidadPacientes] = useState(0);
  const [respaldos, setRespaldos] = useState([]);
  const [loadingTable, setLoadingTable] = useState(true);
  const [loadingRespaldo, setLoadingRespaldo] = useState(false);

  const getHistoryAndNumPacientes = () => {
    setLoadingTable(true);
    PacientesApi.GetAll().then((res) => {
      setCantidadPacientes(res.cantidadPacientes);
    });

    DatosAPI.GetHistory().then((res) => {
      if(res.length){
        setRespaldos(res)
      }
      setLoadingTable(false)
    })
    // .catch(err => {
    //   console.log(err)
    // });
  };

  const crearRespaldo = () =>{
    setLoadingRespaldo(true)
    DatosAPI.CreateBackup().then((res) => {    
      getHistoryAndNumPacientes()
      setLoadingRespaldo(false)
    }).catch(err => {
      console.log(err)
      setLoadingRespaldo(false)
      alert("Hubo un error")
    });;
  }

  const handleModal = () =>{
  //  setLoadingRespaldo(!loadingRespaldo)
  }

  return (
    <Fragment>
      <Form>
    
        <Row>
          <Col md="6">
            <h4>Ultimos 10 respaldos:</h4>
          </Col>
          <Col md="6 ">
            <h4 >Cantidad actual de pacientes: {cantidadPAcientes}</h4>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
          <Table>
              <thead>
                <tr>
                  <th>Nombre Archivo</th>
                  <th># de Pacientes</th>
                </tr>
              </thead>
              <tbody>
                {
                  loadingTable ? (<Spinner> </Spinner>) : (respaldos.map( r => {
                    return(
                      <tr>
                        <td>{r.nombreArchivo}</td>
                        <td>{r.cantidadPacientesRespaldados}</td>
                      </tr>
                    )
                  }))
                }
              </tbody>
            </Table>
          </Col>
          <Col md={6}>
            <Button onClick={crearRespaldo} className="btn-primary">
              Generar
            </Button>
          </Col>
          {/* <Col md={2}>
            <Button className="btn-secundary">
              Cargar
            </Button>
          </Col> */}
        </Row>
      </Form>
      <Modal isOpen={loadingRespaldo} toggle={handleModal}>
          <ModalHeader>Respaldo en curso</ModalHeader>
          <ModalBody>
            El respaldo esta en curso, esto puede tardar un par de minutos, por favor espere.
          </ModalBody>
          <ModalFooter>
          </ModalFooter>
        </Modal>
    </Fragment>
  );
};

export default Respaldo ;
