import React, { useState, useEffect } from "react";
import { PacientesApi } from "../Services/PacientesAPI";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  Navbar,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Container,
} from "reactstrap";
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from "reactstrap";
import classnames from "classnames";
import { useParams } from "react-router-dom";
import swal from "sweetalert";
import InfoGeneral from "../Components/Paciente/InfoGeneral";
import AntPatologicos from "../Components/Paciente/AntPatologicos";
import AntNoPatologicos from "../Components/Paciente/AntNoPatologicos";
import AntHeredoFamiliares from "../Components/Paciente/AntHeredoFamiliares";
import AntGinecoObstetricos from "../Components/Paciente/AntGinecoObstetricos";
import Historial from "../Components/Paciente/Historial";

const Paciente = () => {
  const { PacienteId } = useParams();

  const [activeTab, setActiveTab] = useState("1");
  const [pacienteActual, setPaciente] = useState({});
  const [warningModal, toogleWarning] = useState(false);

  const toggleTab = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  const respaldaPaciente = (paciente) => {
    setPaciente(paciente);
    console.log("Paciente guardado:", paciente);
  };

  const respaldaAntecedente = (antecedente) => {
    setPaciente({
      ...pacienteActual,
      [antecedente.nombre]: { ...antecedente.contenido },
    });
    console.log("Antecedente guardado:", antecedente);
  };

  useEffect(() => {
    if (PacienteId) {
      PacientesApi.Get(PacienteId).then((res) => {
        console.log("Paciente obtenido", res);
        setPaciente(res);
      });
    } else {
      setPaciente({});
    }
  }, [PacienteId]);

  const toogleWarningModal = () => {
    toogleWarning(!warningModal);
  };

  const confirmDelete = () => {
    PacientesApi.EliminaPaciente(PacienteId)
      .then((res) => {
        window.history.back();
      })
      .catch((err) => {
        console.log("Error: ", err);
        swal({
          title: "Error!",
          text: "Hubo un error intentando borrar el paciente",
          icon: "error",
          confirmButtonColor: "#FFF",
        });
      });
  };

  return (
    <Container>
      <Modal isOpen={warningModal} toggle={toogleWarningModal}>
        <ModalHeader toggle={toogleWarningModal}>Advertencia</ModalHeader>
        <ModalBody>¿Esta seguro de querer borrar al paciente?</ModalBody>
        <br />
        <ModalFooter>
          <Button color="primary" onClick={confirmDelete}>
            Si
          </Button>
          <Button onClick={toogleWarningModal}>No</Button>
        </ModalFooter>
      </Modal>
      <h4>
        {PacienteId ? "Editar " : "Crear "}Paciente{" "}
        {(pacienteActual.nombre ? '"' + pacienteActual.nombre : "") +
          (pacienteActual.apellidoP
            ? " " + pacienteActual.apellidoP + ","
            : "") +
          (pacienteActual.edad ? " " + pacienteActual.edad + '"' : "")}
      </h4>
      <Card>
        <Navbar color="light" light expand="md">
          <Nav navbar>
            <NavItem>
              <NavLink
                className={classnames({ active: activeTab === "1" })}
                onClick={() => {
                  toggleTab("1");
                }}
              >
                Paciente
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: activeTab === "5" })}
                onClick={() => {
                  toggleTab("5");
                }}
              >
                Ant. Gineco Obstetricos
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: activeTab === "3" })}
                onClick={() => {
                  toggleTab("3");
                }}
              >
                Ant. No Patologicos
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: activeTab === "2" })}
                onClick={() => {
                  toggleTab("2");
                }}
              >
                Ant. Patologicos
              </NavLink>
            </NavItem>

            <NavItem>
              <NavLink
                className={classnames({ active: activeTab === "4" })}
                onClick={() => {
                  toggleTab("4");
                }}
              >
                Ant. Heredo Familiares
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: activeTab === "6" })}
                onClick={() => {
                  toggleTab("6");
                }}
              >
                Historial
              </NavLink>
            </NavItem>

            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                Opciones
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem disabled>Imprimir expediente</DropdownItem>
                <DropdownItem onClick={toogleWarningModal}>Borrar</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        </Navbar>
        <TabContent activeTab={activeTab}>
          <TabPane tabId="1">
            <InfoGeneral
              paciente={pacienteActual}
              respaldaPaciente={respaldaPaciente}
            />
          </TabPane>
          <TabPane tabId="2">
            <AntPatologicos
              paciente={pacienteActual}
              respaldaAntecedente={respaldaAntecedente}
            />
          </TabPane>
          <TabPane tabId="3">
            <AntNoPatologicos
              paciente={pacienteActual}
              respaldaAntecedente={respaldaAntecedente}
            />
          </TabPane>
          <TabPane tabId="4">
            <AntHeredoFamiliares
              paciente={pacienteActual}
              respaldaAntecedente={respaldaAntecedente}
            />
          </TabPane>
          <TabPane tabId="5">
            <AntGinecoObstetricos
              paciente={pacienteActual}
              respaldaAntecedente={respaldaAntecedente}
            ></AntGinecoObstetricos>
          </TabPane>
          <TabPane tabId="6">
            <Historial
              paciente={pacienteActual}
              respaldaHistorial={respaldaAntecedente}
            ></Historial>
          </TabPane>
        </TabContent>
      </Card>
    </Container>
  );
};

export default Paciente;
