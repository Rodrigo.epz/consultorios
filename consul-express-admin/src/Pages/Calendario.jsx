import React, { useState, useEffect } from "react";
import FullCalendar, { formatDate } from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import ApiCalendar from "react-google-calendar-api/src/ApiCalendar";
import EventModal from "../Components/Calendar/EventModal";
import swal from "sweetalert";
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from "reactstrap";

const Calendario = () => {
  const [events, setEvents] = useState(null);
  const [isModalOpen, setModal] = useState(false);
  const [eventToEdit, setEventToEdit] = useState({});
  const [warningOpen, setWarning] = useState(false);

  useEffect(() => {
    ApiCalendar.onLoad(firstCheck);
  }, []);

  //this function is to ensure the load of gapi
  const firstCheck = () => {
    if (!ApiCalendar.sign) {
      ApiCalendar.handleAuthClick();
    } else {
      ApiCalendar.listUpcomingEvents().then(({ result }) => {
        let events = result.items;

        if (events.length > 0) {
          setEvents(formatEvents(events));
        }
        console.log(result.items);
      });
    }
  };

  const formatEvents = (list) => {
    return list.map((item) => ({
      id: item.id,
      title: item.summary,
      start: item.start.dateTime || item.start.date,
      end: item.end.dateTime || item.end.date,
    }));
  };

  const handleModal = () => {
    setModal(!isModalOpen);
  };

  const handleEventClick = (e) => {
    console.log(e);
    setEventToEdit(e.event);
    handleModal();
  };

  const handleEventSubmit = (data) => {
    console.log(data);
    const startDate = data.start.split("-");
    const newStartDate = new Date(startDate[0], startDate[1] - 1, startDate[2]);
    const newEndDate = new Date(startDate[0], startDate[1] - 1, startDate[2]);

    const startTime = data.startHour.split(":");
    newStartDate.setHours(startTime[0], startTime[1]);

    const endTime = data.endHour.split(":");
    newEndDate.setHours(endTime[0], endTime[1]);
    const event = {
      start: {
        dateTime: newStartDate.toISOString(),
      },
      end: {
        dateTime: newEndDate.toISOString(),
      },
      summary: data.title,
    };

    if (data.id) {
      ApiCalendar.updateEvent(event, data.id)
        .then((res) => {
          swal({
            title: "Bien!",
            text: "Se ah editado una cita",
            icon: "success",
            buttons: {
              confirm: { text: "Ok", className: "btn-primary" },
            },
          });
        })
        .catch((err) => {
          console.log("Error: ", err);
          swal({
            title: "Error!",
            text: "Hubo un error intentando editar la cita",
            icon: "error",
            confirmButtonColor: "#FFF",
          });
        });
    } else {
      ApiCalendar.createEvent(event)
        .then((res) => {
          swal({
            title: "Bien!",
            text: "Se ah creado una cita",
            icon: "success",
            buttons: {
              confirm: { text: "Ok", className: "btn-primary" },
            },
          });
        })
        .catch((err) => {
          console.log("Error: ", err);
          swal({
            title: "Error!",
            text: "Hubo un error intentando crear la cita",
            icon: "error",
            confirmButtonColor: "#FFF",
          });
        });
    }
    setEvents(null);
    handleModal();
    // modifyLocalEvent(data.id, event);
    setTimeout(firstCheck, 1000);
  };

  const handleCreateEvent = (e) => {
    console.log("evento: ", e)
    setEventToEdit(e);
    handleModal();
  };

  const confirmDelete = () => {
    ApiCalendar.deleteEvent(eventToEdit.id).then((res) => {
      swal({
        title: "Bien!",
        text: "Se ah borrado una cita",
        icon: "success",
        buttons: {
          confirm: { text: "Ok", className: "btn-primary" },
        },
      });
    });
    setEvents(null);
    toogleWarning();
    handleModal();
    setTimeout(firstCheck, 1000);
  };

  const toogleWarning = () => {
    setWarning(!warningOpen);
  };

  const modifyLocalEvent = (id, data) => {
    console.log(id, data);

    if (data && data.start && data.end) {
      const eventsCopy = events;
      const index = eventsCopy.findIndex((item) => item.id == id);
      eventsCopy[index].start = data.start.dateTime;
      eventsCopy[index].end = data.end.dateTime;
      eventsCopy[index].title = data.summary;
      console.log(eventsCopy);
      setEvents(eventsCopy);
    } else {
      const eventsCopy = events;
      eventsCopy = eventsCopy.filter((item) => item.id == id);
      console.log(eventsCopy);
      setEvents(eventsCopy);
    }
  };

  return (
    <>
      <EventModal
        isOpen={isModalOpen}
        handleEventSubmit={handleEventSubmit}
        closeModal={handleModal}
        evento={eventToEdit}
        toogleWarning={toogleWarning}
      ></EventModal>
      <Modal isOpen={warningOpen} toggle={toogleWarning}>
        <ModalHeader toggle={toogleWarning}>Advertencia</ModalHeader>
        <ModalBody>¿Esta seguro de querer borrar esta cita?</ModalBody>
        <br />
        <ModalFooter>
          <Button color="primary" onClick={confirmDelete}>
            Si
          </Button>
          <Button onClick={toogleWarning}>No</Button>
        </ModalFooter>
      </Modal>
      {/* <button onClick={(e) => handleItemClick(e, "sign-in")}>sign-in</button>
      <button onClick={(e) => handleItemClick(e, "sign-out")}>sign-out</button> */}
      {/* <Button
        color="primary"
        className="float-right"
        onClick={handleCreateEvent}
      >
        crear evento
      </Button> */}
      <FullCalendar
        plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
        headerToolbar={{
          left: "prev,next today",
          center: "title",
          right: "dayGridMonth,timeGridWeek,timeGridDay",
        }}
        buttonText={{
          today: "hoy",
          month: "mes",
          week: "semana",
          day: "dia",
          list: "lista",
        }}
        initialView="dayGridMonth"
        weekends={true}
        events={events}
        initialView="dayGridMonth"
        editable={true}
        selectable={true}
        selectMirror={true}
        dayMaxEvents={true}
        weekends={true}
        initialEvents={[]}
        locale="es"
        eventClick={handleEventClick}
        dateClick={handleCreateEvent}
      />
    </>
  );
};

export default Calendario;
