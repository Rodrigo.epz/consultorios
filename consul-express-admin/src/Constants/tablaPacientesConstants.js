import { Link } from "react-router-dom";
import editLogo from "../resources/edit_24.svg";

export const columns = [
  {
    Header: "Paciente",
    sticky: "left",

    columns: [
      {
        Header: "",
        accessor: "id",
        Cell: (props) => {
          return (
            <Link to={`alta-pacientes/${props.value}`}>
              {" "}
              <img src={editLogo}></img>{" "}
            </Link>
          );
        },
        disableSortBy: true,
      },
      {
        Header: "Nombre",
        accessor: "nombre", // accessor is the "key" in the data
      },
      {
        Header: "Apellido P",
        accessor: "apellidoP",
      },
      {
        Header: "Apellido M",
        accessor: "apellidoM",
      },
      {
        Header: "Edad",
        accessor: "edad",
      },
      {
        Header: "Fecha Nacimiento",
        accessor: "fechaNacimiento",
        // Cell: (props) => {
        //   return (
        //     <div>
        //       {" "}
        //       {new Date(
        //         props.value.split("T")[0].split("-")
        //       ).toLocaleDateString()}{" "}
        //     </div>
        //   );
        // },
      },
      {
        Header: "Telefono",
        accessor: "telefono",
      },
      {
        Header: "Direccion",
        accessor: "direccion",
      },
      {
        Header: "Motivo Consulta",
        accessor: "motivoConsulta",
         Cell: (props) => {
          return (
            <div className="consul-dashboard-table-motivoConsul">
              {" "}
              {props.value}{" "}
            </div>
          );
        },
      },
    ],
  },
  {
    Header: "Informacion Medica",
    columns: [
      {
        Header: "Alcoholismo",
        accessor: "antPatologicos.alcoholismo",
        disableSortBy: true,
      },
      {
        Header: "Fumador",
        accessor: "antPatologicos.fumador",
        disableSortBy: true,
      },
      {
        Header: "Alergias",
        accessor: "antPatologicos.alergias",
        disableSortBy: true,
      },
    ],
  },
];
