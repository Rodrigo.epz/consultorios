﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using consul_express_api.Models.DB;
using consul_express_api.Helpers;

namespace consul_express_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AntNOPatologicosController : ControllerBase
    {
        private readonly ConsultorioDBContext _context;

        public AntNOPatologicosController(ConsultorioDBContext context)
        {
            _context = context;
        }

        // GET: api/AntNOPatologicos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AntNOPatologicos>>> GetAntNOPatologicos()
        {
            return await _context.AntNOPatologicos.ToListAsync();
        }

        // GET: api/AntNOPatologicos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AntNOPatologicos>> GetAntNOPatologicos(int id)
        {
            var antNOPatologicos = await _context.AntNOPatologicos.FindAsync(id);

            if (antNOPatologicos == null)
            {
                return NotFound();
            }

            return antNOPatologicos;
        }

        // PUT: api/AntNOPatologicos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<IActionResult> PutAntNOPatologicos(AntNOPatologicos antNOPatologicos)
        {
            if (antNOPatologicos.Id == 0)
            {
                return BadRequest(string.Format(Constantes.XDebeSerMayor0, nameof(antNOPatologicos.Id)));
            }

            _context.Entry(antNOPatologicos).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AntNOPatologicosExists(antNOPatologicos.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(antNOPatologicos);
        }

        // POST: api/AntNOPatologicos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<AntNOPatologicos>> PostAntNOPatologicos(AntNOPatologicos antNOPatologicos)
        {
            _context.AntNOPatologicos.Add(antNOPatologicos);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAntNOPatologicos", new { id = antNOPatologicos.Id }, antNOPatologicos);
        }

        // DELETE: api/AntNOPatologicos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AntNOPatologicos>> DeleteAntNOPatologicos(int id)
        {
            var antNOPatologicos = await _context.AntNOPatologicos.FindAsync(id);
            if (antNOPatologicos == null)
            {
                return NotFound();
            }

            _context.AntNOPatologicos.Remove(antNOPatologicos);
            await _context.SaveChangesAsync();

            return antNOPatologicos;
        }

        private bool AntNOPatologicosExists(int id)
        {
            return _context.AntNOPatologicos.Any(e => e.Id == id);
        }
    }
}
