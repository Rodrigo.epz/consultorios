﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using consul_express_api.Models.DB;
using consul_express_api.Helpers;

namespace consul_express_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AntHeredoFamiliaresController : ControllerBase
    {
        private readonly ConsultorioDBContext _context;

        public AntHeredoFamiliaresController(ConsultorioDBContext context)
        {
            _context = context;
        }

        // GET: api/AntHeredoFamiliares
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AntHeredoFamiliares>>> GetAntHeredoFamiliares()
        {
            return await _context.AntHeredoFamiliares.ToListAsync();
        }

        // GET: api/AntHeredoFamiliares/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AntHeredoFamiliares>> GetAntHeredoFamiliares(int id)
        {
            var antHeredoFamiliares = await _context.AntHeredoFamiliares.FindAsync(id);

            if (antHeredoFamiliares == null)
            {
                return NotFound();
            }

            return antHeredoFamiliares;
        }

        // PUT: api/AntHeredoFamiliares/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<IActionResult> PutAntHeredoFamiliares(AntHeredoFamiliares antHeredoFamiliares)
        {
            if (antHeredoFamiliares.Id == 0)
            {
                return BadRequest(string.Format(Constantes.XDebeSerMayor0, nameof(antHeredoFamiliares.Id)));
            }

            _context.Entry(antHeredoFamiliares).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AntHeredoFamiliaresExists(antHeredoFamiliares.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(antHeredoFamiliares);
        }

        // POST: api/AntHeredoFamiliares
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<AntHeredoFamiliares>> PostAntHeredoFamiliares(AntHeredoFamiliares antHeredoFamiliares)
        {
            _context.AntHeredoFamiliares.Add(antHeredoFamiliares);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAntHeredoFamiliares", new { id = antHeredoFamiliares.Id }, antHeredoFamiliares);
        }

        // DELETE: api/AntHeredoFamiliares/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AntHeredoFamiliares>> DeleteAntHeredoFamiliares(int id)
        {
            var antHeredoFamiliares = await _context.AntHeredoFamiliares.FindAsync(id);
            if (antHeredoFamiliares == null)
            {
                return NotFound();
            }

            _context.AntHeredoFamiliares.Remove(antHeredoFamiliares);
            await _context.SaveChangesAsync();

            return antHeredoFamiliares;
        }

        private bool AntHeredoFamiliaresExists(int id)
        {
            return _context.AntHeredoFamiliares.Any(e => e.Id == id);
        }
    }
}
