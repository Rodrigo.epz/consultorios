﻿using consul_express_api.Models.DB;
using consul_express_api.Models.Schemas;
using consul_express_api.Services;
using consul_express_api.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NPOI.HPSF;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Metadata;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DatosController : ApiBase
    {
        private ConsultorioDBContext _context;
        IWebHostEnvironment _environment;
        private IRespaldoService _respaldoService;

        public DatosController(ILogger<ApiBase> logger, IHttpContextAccessor httpContextAccessor, ConsultorioDBContext context, IWebHostEnvironment environment, IRespaldoService respaldoService) : base(logger, httpContextAccessor)
        {
            _context = context;
            _environment = environment;
            _respaldoService = respaldoService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                return Ok(await _respaldoService.GetAll());
            }
            catch(Exception ex) 
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("save")]
        public async Task<IActionResult> SaveFile()//En endpoint devolvera un base64, ya que no se pudo desarrollar de parte del front como desecncriptar el file enduro
        {
            try
            {

                var date = DateTime.UtcNow;//Convertir cuando sea administrable el dia
                string sql = "EXEC GetAllPacientesInfo";
                                var pacientes = _context.Set<PacientesAllInfo>().FromSqlRaw(sql).ToList();
                string base64;
                if (pacientes == null || pacientes.Count == 0)
                {
                    return NotFound("No hay Pacientes que exportar");
                }
                
                string fileName = "pacientes_" + DateTime.Now.ToString("yyyy-M-d") + ".xlsx";
                string webRootPath = _environment.ContentRootPath + @"\Files\Consolidados";
                //Check if directory exist
                if (!System.IO.Directory.Exists(webRootPath))
                {
                    System.IO.Directory.CreateDirectory(webRootPath); //Create directory if it doesn't exist
                }

                IWorkbook workbook = new XSSFWorkbook();

                using (var fs = new FileStream(Path.Combine(webRootPath, fileName), FileMode.OpenOrCreate, System.IO.FileAccess.Write))
                {
                    ISheet excelSheet = workbook.CreateSheet("Pacientes");
                  
                    IRow row = excelSheet.CreateRow(0);

                    row.CreateCell(0).SetCellValue(("Id"));
                    row.CreateCell(1).SetCellValue(("Nombre"));
                    row.CreateCell(2).SetCellValue(("Apellido Paterno"));
                    row.CreateCell(3).SetCellValue(("Apellido Materno"));
                    row.CreateCell(4).SetCellValue(("Direccion"));
                    row.CreateCell(5).SetCellValue(("Edad"));
                    row.CreateCell(6).SetCellValue(("Telefono"));
                    row.CreateCell(7).SetCellValue(("Motivo de Consulta"));
                    row.CreateCell(8).SetCellValue(("Fecha de Nacimiento"));
                    row.CreateCell(9).SetCellValue(("Lotus"));
                    row.CreateCell(10).SetCellValue(("AntPatId"));
                    row.CreateCell(11).SetCellValue(("Fumador"));
                    row.CreateCell(12).SetCellValue(("Alcoholismo"));
                    row.CreateCell(13).SetCellValue(("Alergias"));
                    row.CreateCell(14).SetCellValue(("Fracturas"));
                    row.CreateCell(15).SetCellValue(("Transfusiones"));
                    row.CreateCell(16).SetCellValue(("Cirugias"));
                    row.CreateCell(17).SetCellValue(("Enfermedades"));
                    row.CreateCell(18).SetCellValue(("GrupoRH"));
                    row.CreateCell(19).SetCellValue(("AntNoPatId"));
                    row.CreateCell(20).SetCellValue(("EstadoCivil"));
                    row.CreateCell(21).SetCellValue(("Ocupacion"));
                    row.CreateCell(22).SetCellValue(("Habitos, Higiene, Dieta"));
                    row.CreateCell(23).SetCellValue(("HeredId"));
                    row.CreateCell(24).SetCellValue(("Cancer"));
                    row.CreateCell(25).SetCellValue(("Tuberculosis"));
                    row.CreateCell(26).SetCellValue(("Asma"));
                    row.CreateCell(27).SetCellValue(("EnfCronicoDegenerativas"));
                    row.CreateCell(28).SetCellValue(("GineId"));
                    row.CreateCell(29).SetCellValue(("Monarca"));
                    row.CreateCell(30).SetCellValue(("Ritmo"));
                    row.CreateCell(31).SetCellValue(("Eumenorreica"));
                    row.CreateCell(32).SetCellValue(("Dismenorreica"));
                    row.CreateCell(33).SetCellValue(("IVSA"));
                    row.CreateCell(34).SetCellValue(("Gesta"));
                    row.CreateCell(35).SetCellValue(("Para"));
                    row.CreateCell(36).SetCellValue(("Abortos"));
                    row.CreateCell(37).SetCellValue(("Cesareas"));
                    row.CreateCell(38).SetCellValue(("Fup"));
                    row.CreateCell(39).SetCellValue(("Fuc"));
                    row.CreateCell(40).SetCellValue(("Fua"));
                    row.CreateCell(41).SetCellValue(("Fecha de Primer Parto"));
                    row.CreateCell(42).SetCellValue(("Mpf"));
                    row.CreateCell(43).SetCellValue(("Doc"));
                    row.CreateCell(44).SetCellValue(("Fum"));
                    row.CreateCell(45).SetCellValue(("Fpp"));
                    row.CreateCell(46).SetCellValue(("HistoId"));
                    row.CreateCell(47).SetCellValue(("Subjetivo"));
                    row.CreateCell(48).SetCellValue(("Laboratorio Y Gabinete"));
                    row.CreateCell(49).SetCellValue(("Objetivo"));
                    row.CreateCell(50).SetCellValue(("Analisis IDX"));
                    row.CreateCell(51).SetCellValue(("Plan"));

                    //styling
                    var boldFont = workbook.CreateFont();
                    boldFont.Boldweight = (short)FontBoldWeight.Bold;
                    ICellStyle boldStyle = workbook.CreateCellStyle();
                    boldStyle.SetFont(boldFont);

                    boldStyle.BorderBottom = BorderStyle.Medium;
                    boldStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
                    boldStyle.FillPattern = FillPattern.SolidForeground;

                    row.Cells.ForEach(cell => cell.CellStyle = boldStyle);

                    int rowIndex = 1;

                    foreach (var paciente in pacientes)
                    {
                        row = excelSheet.CreateRow(rowIndex);

                        row.CreateCell(0).SetCellValue(paciente.Id.ToString());//ID
                        row.CreateCell(1).SetCellValue((paciente.Nombre));
                        row.CreateCell(2).SetCellValue((paciente.ApellidoP));
                        row.CreateCell(3).SetCellValue((paciente.ApellidoM));
                        row.CreateCell(4).SetCellValue((paciente.Direccion));
                        row.CreateCell(5).SetCellValue((paciente.Edad));
                        row.CreateCell(6).SetCellValue((paciente.Telefono));
                        row.CreateCell(7).SetCellValue((paciente.MotivoConsulta));
                        row.CreateCell(8).SetCellValue((paciente.FechaNacimiento.ToString("dd/MM/yyyy")));
                        row.CreateCell(9).SetCellValue((paciente.Lotus ? "Si" : "No"));
                        row.CreateCell(10).SetCellValue((paciente.AntPatId ?? 0));
                        row.CreateCell(11).SetCellValue((paciente.Fumador));
                        row.CreateCell(12).SetCellValue((paciente.Alcoholismo));
                        row.CreateCell(13).SetCellValue((paciente.Alergias));
                        row.CreateCell(14).SetCellValue((paciente.Fracturas));
                        row.CreateCell(15).SetCellValue((paciente.Transfusiones));
                        row.CreateCell(16).SetCellValue((paciente.Cirugias));
                        row.CreateCell(17).SetCellValue((paciente.Enfermedades));
                        row.CreateCell(18).SetCellValue((paciente.GrupoRH));
                        row.CreateCell(19).SetCellValue((paciente.AntNoPatId ?? 0));
                        row.CreateCell(20).SetCellValue((paciente.EstadoCivil));
                        row.CreateCell(21).SetCellValue((paciente.Ocupacion));
                        row.CreateCell(22).SetCellValue((paciente.HabitosHigieneDieta));
                        row.CreateCell(23).SetCellValue((paciente.HeredId ?? 0));
                        row.CreateCell(24).SetCellValue((paciente.Cancer));
                        row.CreateCell(25).SetCellValue((paciente.Tuberculosis));
                        row.CreateCell(26).SetCellValue((paciente.Asma));
                        row.CreateCell(27).SetCellValue((paciente.EnfCronicoDegenerativas));
                        row.CreateCell(28).SetCellValue((paciente.GineId ?? 0));
                        row.CreateCell(29).SetCellValue((paciente.Monarca ?? 0));
                        row.CreateCell(30).SetCellValue((paciente.Ritmo));
                        row.CreateCell(31).SetCellValue((paciente.Eumenorreica != null && paciente.Eumenorreica.Value ? "Si" : "No" ));
                        row.CreateCell(32).SetCellValue((paciente.Dismenorreica != null && paciente.Dismenorreica.Value  ? "Si": "No"));
                        row.CreateCell(33).SetCellValue((paciente.IVSA ?? 0));
                        row.CreateCell(34).SetCellValue((paciente.Gesta ?? 0));
                        row.CreateCell(35).SetCellValue((paciente.Para ?? 0));
                        row.CreateCell(36).SetCellValue((paciente.Abortos ?? 0));
                        row.CreateCell(37).SetCellValue((paciente.Cesareas ?? 0));
                        row.CreateCell(38).SetCellValue((paciente.Fup));
                        row.CreateCell(39).SetCellValue((paciente.Fuc));
                        row.CreateCell(40).SetCellValue((paciente.Fua));
                        row.CreateCell(41).SetCellValue((paciente.FechaPrimerParto));
                        row.CreateCell(42).SetCellValue((paciente.Mpf));
                        row.CreateCell(43).SetCellValue((paciente.Doc));
                        row.CreateCell(44).SetCellValue((paciente.Fum));
                        row.CreateCell(45).SetCellValue((paciente.Fpp));
                        row.CreateCell(46).SetCellValue((paciente.HistoId ?? 0));
                        row.CreateCell(47).SetCellValue((paciente.Subjetivo));
                        row.CreateCell(48).SetCellValue((paciente.LaboratorioYGabinete));
                        row.CreateCell(49).SetCellValue((paciente.Objetivo));
                        row.CreateCell(50).SetCellValue((paciente.AnalisisIDX));
                        row.CreateCell(51).SetCellValue((paciente.Plan));

                        rowIndex++;
                    }

                    for (int i = 0; i < 52; i++)
                    {
                        excelSheet.AutoSizeColumn(i);
                    }
                    excelSheet.SetAutoFilter(new CellRangeAddress(0, pacientes.Count(), 0, 51));
                    workbook.Write(fs, false);
                }

                await _respaldoService.Save(new RespaldoSchema()
                {
                    NombreArchivo = fileName,
                    CantidadPacientesRespaldados = pacientes.Count
                });

                using (var ms = new MemoryStream())
                {
                    workbook.Write(ms, false);
                    workbook.Dispose();
                    return File(ms.ToArray(), "application/vnd.ms-excel", fileName);
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return BadRequest("Hubo un problema");
        }

    }
}
