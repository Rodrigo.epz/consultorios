﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using consul_express_api.Models.Schemas;
using consul_express_api.Services.Interfaces;
using consul_express_api.Helpers.Exceptions;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Logging;
using consul_express_api.Helpers;

namespace consul_express_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PacientesController : ApiBase
    {
        private IPacienteService _pacienteService;

        public PacientesController(ILogger<ApiBase> logger, IHttpContextAccessor httpContextAccessor, IPacienteService pacienteService) : base(logger, httpContextAccessor)
        {
            _pacienteService = pacienteService;
        }

        // GET: api/Pacientes
        [HttpGet]
        public async Task<ActionResult<PacientesPaginado>> GetPacientes([FromQuery]ParametrosBusqueda parametros)
        {
            return Ok(await _pacienteService.GetAll(parametros));
        }

        // GET: api/Pacientes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PacienteSchema>> GetPaciente(Guid id)
        {
            try
            {
                var paciente = await _pacienteService.Get(id);
                return Ok(paciente);
            }
            catch(EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
         
        }

        // PUT: api/Pacientes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<ActionResult<PacienteSchema>> PutPaciente(PacienteSchema paciente)
        {
            if (paciente.Id == null || paciente.Id == Guid.Empty)
            {
                return BadRequest(string.Format(Constantes.XNoPuedeSerY, nameof(paciente.Id), "null"));
            }

            try
            {
                var nuevoPaciente = await _pacienteService.Update(paciente);
                return Ok(nuevoPaciente);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return BadRequest();
            }
        }

        // POST: api/Pacientes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<PacienteSchema>> PostPaciente([FromBody]PacienteSchema paciente)
        {
            try
            {
                var id = await _pacienteService.Add(paciente);
                paciente.Id = id;

                return CreatedAtAction(nameof(GetPaciente), new { id = id }, paciente);
            }
            catch(DuplicateEntityException ex)
            {
                return Conflict(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/Pacientes/5?borradoLogico
        [HttpDelete("{id}")]
        public async Task<ActionResult<PacienteSchema>> DeletePaciente(Guid id, bool borradoLogico = true)
        {
            try
            {
                var paciente = await _pacienteService.Delete(id, borradoLogico);
                return paciente;
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

    }
}
