﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using consul_express_api.Models.DB;
using consul_express_api.Helpers;
using Microsoft.Extensions.Logging;

namespace consul_express_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AntGinecoObstetricosController : ApiBase
    {
        private readonly ConsultorioDBContext _context;

        public AntGinecoObstetricosController(ILogger<ApiBase> logger, IHttpContextAccessor httpContextAccessor, ConsultorioDBContext context): base(logger, httpContextAccessor)
        {
            _context = context;
        }

        // GET: api/AntGinecoObstetricos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AntGinecoObstetricos>>> GetAntGinecoObstetricos()
        {
            return await _context.AntGinecoObstetricos.ToListAsync();
        }

        // GET: api/AntGinecoObstetricos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AntGinecoObstetricos>> GetAntGinecoObstetricos(int id)
        {
            var antGinecoObstetricos = await _context.AntGinecoObstetricos.FindAsync(id);

            if (antGinecoObstetricos == null)
            {
                return NotFound();
            }

            return antGinecoObstetricos;
        }

        // PUT: api/AntGinecoObstetricos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<IActionResult> PutAntGinecoObstetricos(AntGinecoObstetricos antGinecoObstetricos)
        {
            if (antGinecoObstetricos.Id == 0)
            {
                return BadRequest(string.Format(Constantes.XDebeSerMayor0, nameof(antGinecoObstetricos.Id)));
            }

            _context.Entry(antGinecoObstetricos).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AntGinecoObstetricosExists(antGinecoObstetricos.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(antGinecoObstetricos);
        }

        // POST: api/AntGinecoObstetricos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<AntGinecoObstetricos>> PostAntGinecoObstetricos(AntGinecoObstetricos antGinecoObstetricos)
        {
            _context.AntGinecoObstetricos.Add(antGinecoObstetricos);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAntGinecoObstetricos", new { id = antGinecoObstetricos.Id }, antGinecoObstetricos);
        }

        // DELETE: api/AntGinecoObstetricos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AntGinecoObstetricos>> DeleteAntGinecoObstetricos(int id)
        {
            var antGinecoObstetricos = await _context.AntGinecoObstetricos.FindAsync(id);
            if (antGinecoObstetricos == null)
            {
                return NotFound();
            }

            _context.AntGinecoObstetricos.Remove(antGinecoObstetricos);
            await _context.SaveChangesAsync();

            return antGinecoObstetricos;
        }

        private bool AntGinecoObstetricosExists(int id)
        {
            return _context.AntGinecoObstetricos.Any(e => e.Id == id);
        }
    }
}
