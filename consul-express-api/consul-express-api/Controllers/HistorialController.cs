﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using consul_express_api.Models.DB;
using Microsoft.Extensions.Logging;
using consul_express_api.Helpers;

namespace consul_express_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HistorialController : ApiBase
    {
        private readonly ConsultorioDBContext _context;

        public HistorialController(ILogger<ApiBase> logger, IHttpContextAccessor httpContextAccessor, ConsultorioDBContext context) : base(logger, httpContextAccessor)
        {
            _context = context;
        }

        // GET: api/Historial
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Historial>>> GetHistorial()
        {
            return await _context.Historial.ToListAsync();
        }

        // GET: api/Historial/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Historial>> GetHistorial(int id)
        {
            var historial = await _context.Historial.FindAsync(id);

            if (historial == null)
            {
                return NotFound();
            }

            return historial;
        }

        // PUT: api/Historial/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<IActionResult> PutHistorial(Historial historial)
        {
            if (historial.Id == 0)
            {
                return BadRequest(string.Format(Constantes.XDebeSerMayor0, nameof(historial.Id)));
            }

            _context.Entry(historial).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HistorialExists(historial.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(historial);
        }

        // POST: api/Historial
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Historial>> PostHistorial(Historial historial)
        {
            _context.Historial.Add(historial);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetHistorial", new { id = historial.Id }, historial);
        }

        // DELETE: api/Historial/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Historial>> DeleteHistorial(int id)
        {
            var historial = await _context.Historial.FindAsync(id);
            if (historial == null)
            {
                return NotFound();
            }

            _context.Historial.Remove(historial);
            await _context.SaveChangesAsync();

            return historial;
        }

        private bool HistorialExists(int id)
        {
            return _context.Historial.Any(e => e.Id == id);
        }
    }
}
