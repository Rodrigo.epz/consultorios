﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using consul_express_api.Models.DB;
using Microsoft.Extensions.Logging;
using consul_express_api.Helpers;

namespace consul_express_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AntPatologicosController : ApiBase
    {
        private readonly ConsultorioDBContext _context;

        public AntPatologicosController(ILogger<ApiBase> logger, IHttpContextAccessor httpContextAccessor, ConsultorioDBContext context):base(logger, httpContextAccessor)
        {
            _context = context;
        }

        // GET: api/AntPatologicos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AntPatologicos>>> GetAntPatologicos()
        {
            return await _context.AntPatologicos.ToListAsync();
        }

        // GET: api/AntPatologicos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AntPatologicos>> GetAntPatologicos(int id)
        {
            var antPatologicos = await _context.AntPatologicos.FindAsync(id);

            if (antPatologicos == null)
            {
                return NotFound();
            }

            return antPatologicos;
        }

        // PUT: api/AntPatologicos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<IActionResult> PutAntPatologicos(AntPatologicos antPatologicos)
        {
            if (antPatologicos.Id == 0)
            {
                return BadRequest(string.Format(Constantes.XDebeSerMayor0, nameof(antPatologicos.Id)));
            }

            _context.Entry(antPatologicos).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AntPatologicosExists(antPatologicos.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(antPatologicos);
        }

        // POST: api/AntPatologicos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<AntPatologicos>> PostAntPatologicos(AntPatologicos antPatologicos)
        {
            _context.AntPatologicos.Add(antPatologicos);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAntPatologicos", new { id = antPatologicos.Id }, antPatologicos);
        }

        // DELETE: api/AntPatologicos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AntPatologicos>> DeleteAntPatologicos(int id)
        {
            var antPatologicos = await _context.AntPatologicos.FindAsync(id);
            if (antPatologicos == null)
            {
                return NotFound();
            }

            _context.AntPatologicos.Remove(antPatologicos);
            await _context.SaveChangesAsync();

            return antPatologicos;
        }

        private bool AntPatologicosExists(int id)
        {
            return _context.AntPatologicos.Any(e => e.Id == id);
        }
    }
}
