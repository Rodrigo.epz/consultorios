﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Models.Schemas
{
    public class ParametrosBusqueda
    {
        public int pagina { get; set; } = 0;
        public int itemsPorPagina { get; set; } = 10;
        public string ordenaPor { get; set; }
        public string ordenaDireccion { get; set; }
        public string general { get; set; }
        public string nombre { get; set; }
        public string apellidoPaterno { get; set; }
        public string apellidoMaterno { get; set; }
        public string motivo { get; set; }
    }
}
