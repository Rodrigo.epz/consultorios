﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Models.Schemas
{
    public class AntPatologicoSchema
    {
        public int Id { get; set; }

        public string Fumador { get; set; }
        public string Alcoholismo { get; set; }
        public string Alergias { get; set; }
        public string Fracturas { get; set; }
        public string Transfusiones { get; set; }
        public string Cirugias { get; set; }
        public string Enfermedades { get; set; }
        public string GrupoRH { get; set; }

        [Required]
        public Guid PacienteId { get; set; }
    }
}
