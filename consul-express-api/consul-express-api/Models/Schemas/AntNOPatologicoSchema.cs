﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Models.Schemas
{
    public class AntNOPatologicoSchema
    {
        public int Id { get; set; }

        public string EstadoCivil { get; set; }
        public string Ocupacion { get; set; }
        public string HabitosHigieneDieta { get; set; }

        [Required]
        public Guid PacienteId { get; set; }
    }
}
