﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Models.Schemas
{
    public class AntHeredoFamiliaresSchema
    {
        public int Id { get; set; }

        public string Cancer { get; set; }
        public string Tuberculosis { get; set; }
        public string Asma { get; set; }

        [MaxLength(1000)]
        public string EnfCronicoDegenerativas { get; set; }

        [Required]
        public Guid PacienteId { get; set; }
    }
}
