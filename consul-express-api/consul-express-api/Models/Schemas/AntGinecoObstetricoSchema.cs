﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Models.Schemas
{
    public class AntGinecoObstetricoSchema
    {
        public int Id { get; set; }

        public short Monarca { get; set; }
        public string Ritmo { get; set; }
        public bool Eumenorreica { get; set; }
        public bool Dismenorreica { get; set; }

        public short IVSA { get; set; }
        public short Gesta { get; set; }
        public short Para { get; set; }
        public short Abortos { get; set; }
        public short Cesareas { get; set; }

        public string Fup { get; set; }
        public string Fuc { get; set; }
        public string Fua { get; set; }
        public string FechaPrimerParto { get; set; }

        public string Mpf { get; set; }
        public string Doc { get; set; }
        public string Fum { get; set; }
        public string Fpp { get; set; }

        [Required]
        public Guid PacienteId { get; set; }
    }
}
