﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Models.Schemas
{
    public class PacientesAllInfo
    {
        public Guid Id { get; set; }
        public string Nombre { get; set; }
        public string ApellidoP { get; set; }
        public string ApellidoM { get; set; }
        public string Direccion { get; set; }
        public short  Edad { get; set; }
        public string Telefono { get; set; }

        [MaxLength(1000)]
        public string MotivoConsulta { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public bool Lotus { get; set; }


        public int? AntPatId { get; set; }

        [MaxLength(100)]
        public string Fumador { get; set; }

        [MaxLength(15)]
        public string Alcoholismo { get; set; }
        public string Alergias { get; set; }
        public string Fracturas { get; set; }
        public string Transfusiones { get; set; }
        public string Cirugias { get; set; }
        public string Enfermedades { get; set; }

        [MaxLength(15)]
        public string GrupoRH { get; set; }


        public int? AntNoPatId { get; set; }
        [MaxLength(20)]
        public string EstadoCivil { get; set; }

        [MaxLength(20)]
        public string Ocupacion { get; set; }

        [MaxLength(10)]
        public string HabitosHigieneDieta { get; set; }


        public int? HeredId { get; set; }
        public string Cancer { get; set; }

        [MaxLength(30)]
        public string Tuberculosis { get; set; }

        [MaxLength(30)]
        public string Asma { get; set; }

        [MaxLength(1000)]
        public string EnfCronicoDegenerativas { get; set; }


        public int? GineId { get; set; }
        public short? Monarca { get; set; }

        [MaxLength(15)]
        public string Ritmo { get; set; }
        public bool? Eumenorreica { get; set; }
        public bool? Dismenorreica { get; set; }

        public short? IVSA { get; set; }
        public short? Gesta { get; set; }
        public short? Para { get; set; }
        public short? Abortos { get; set; }
        public short? Cesareas { get; set; }

        public string Fup { get; set; }
        public string Fuc { get; set; }
        public string Fua { get; set; }
        public string FechaPrimerParto { get; set; }

        [MaxLength(50)]
        public string Mpf { get; set; }
        public string Doc { get; set; }
        public string Fum { get; set; }
        public string Fpp { get; set; }



        public int? HistoId { get; set; }
        [MaxLength(10000)]
        public string Subjetivo { get; set; }

        [MaxLength(10000)]
        public string LaboratorioYGabinete { get; set; }

        [MaxLength(10000)]
        public string Objetivo { get; set; }

        [MaxLength(10000)]
        public string AnalisisIDX { get; set; }

        [MaxLength(10000)]
        public string Plan { get; set; }
    }
}
