﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Models.Schemas
{
    public class PacientesPaginado
    {
        public PacientesPaginado() { }

        public PacientesPaginado(IEnumerable<PacienteSchema> pacientes, int cantidadPacientes, int itemsPorPagina)
        {
            CantidadPaginas = (int)Math.Ceiling((decimal)cantidadPacientes / itemsPorPagina);
            CantidadPacientes = cantidadPacientes;
            Pacientes = pacientes;
        }
        public int CantidadPaginas { get; set; }
        public int CantidadPacientes { get; set; }
        public IEnumerable<PacienteSchema> Pacientes { get; set; }
    }
}
