﻿using System;

namespace consul_express_api.Models.Schemas
{
    public class RespaldoSchema
    {
        public string NombreArchivo { get; set; }
        public int CantidadPacientesRespaldados { get; set; }
    }
}
