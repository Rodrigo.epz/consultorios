﻿using System;

namespace consul_express_api.Models.DB
{
    public class RespaldoHistorial
    {
        public int Id { get; set; }
        public string NombreArchivo { get; set; }
        public int CantidadPacientesRespaldados { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}
