﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Models.DB
{
    public class Historial
    {
        public int Id { get; set; }

        [MaxLength(10000)]
        public string Subjetivo { get; set; }

        [MaxLength(10000)]
        public string LaboratorioYGabinete { get; set; }

        [MaxLength(10000)]
        public string Objetivo { get; set; }

        [MaxLength(10000)]
        public string AnalisisIDX { get; set; }

        [MaxLength(10000)]
        public string Plan { get; set; }


        public Guid PacienteId { get; set; }
    }
}
