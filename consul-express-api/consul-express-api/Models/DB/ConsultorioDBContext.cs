﻿using consul_express_api.Models.Schemas;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Models.DB
{
    public class ConsultorioDBContext : DbContext
    {
        public ConsultorioDBContext(DbContextOptions<ConsultorioDBContext> options)
         : base(options)
        {
        }


        //Forma para poder usar DbSet con una entidad sin crear una tabla en la BD
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PacientesAllInfo>().HasNoKey().ToView(null);
        }

        public DbSet<Paciente> Pacientes { get; set; }
        public DbSet<AntNOPatologicos> AntNOPatologicos { get; set; }
        public DbSet<AntPatologicos> AntPatologicos { get; set; }
        public DbSet<AntHeredoFamiliares> AntHeredoFamiliares { get; set; }
        public DbSet<AntGinecoObstetricos> AntGinecoObstetricos { get; set; }
        public DbSet<Historial> Historial { get; set; }
        public DbSet<PacientesAllInfo> PacientesAllInfo { get; set; }
        public DbSet<RespaldoHistorial> RespaldoHistorial{ get; set; }
    }
}
