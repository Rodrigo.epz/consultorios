﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Models.DB
{
    public class AntHeredoFamiliares
    {
        public int Id { get; set; }

        public string Cancer { get; set; }

        [MaxLength(30)]
        public string Tuberculosis { get; set; }
        
        [MaxLength(30)]
        public string Asma { get; set; }
        
        [MaxLength(1000)]
        public string EnfCronicoDegenerativas { get; set; }


        public Guid PacienteId { get; set; }
    }
}
