﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Models.DB
{
    public class Paciente
    {
        public Guid Id { get; set; }

        [Required]
        public string Nombre { get; set; }

        [Required]
        public string ApellidoP { get; set; }

        public string ApellidoM { get; set; }

        [Required]
        public string Direccion { get; set; }

        [Required]
        public short Edad { get; set; }

        public string Telefono { get; set; }

        [MaxLength(1000)]
        public string MotivoConsulta { get; set; }

        [Required]
        public DateTime FechaNacimiento { get; set; }

        [Required]
        public DateTime FechaRegistro { get; set; }

        [Required]
        public bool Lotus { get; set; }

        public AntPatologicos AntPatologicos { get; set; }
        public AntNOPatologicos AntNOPatologicos { get; set; }
        public AntHeredoFamiliares AntHeredoFamiliares { get; set; }
        public AntGinecoObstetricos AntGinecoObstetricos { get; set; }
        public Historial Historial { get; set; }
    }
}
