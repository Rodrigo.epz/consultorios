﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Models.DB
{
    public class AntNOPatologicos
    {
        public int Id { get; set; }

        [MaxLength(20)]
        public string EstadoCivil { get; set; }
        
        [MaxLength(20)]
        public string Ocupacion { get; set; }
        
        [MaxLength(10)]
        public string HabitosHigieneDieta { get; set; }


        public Guid PacienteId { get; set; }

    }
}
