﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Models.DB
{
    public class AntPatologicos
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string Fumador { get; set; }

        [MaxLength(15)]
        public string Alcoholismo { get; set; }
        public string Alergias { get; set; }
        public string Fracturas { get; set; }
        public string Transfusiones { get; set; }
        public string Cirugias { get; set; }
        public string Enfermedades { get; set; }

        [MaxLength(15)]
        public string GrupoRH { get; set; }


        public Guid PacienteId { get; set; }
    }
}
