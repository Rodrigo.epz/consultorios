﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace consul_express_api.Migrations
{
    public partial class addingrelacionespaciente : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_AntPatologicos_PacienteId",
                table: "AntPatologicos");

            migrationBuilder.DropIndex(
                name: "IX_AntNOPatologicos_PacienteId",
                table: "AntNOPatologicos");

            migrationBuilder.DropIndex(
                name: "IX_AntHeredoFamiliares_PacienteId",
                table: "AntHeredoFamiliares");

            migrationBuilder.DropIndex(
                name: "IX_AntGinecoObstetricos_PacienteId",
                table: "AntGinecoObstetricos");

            migrationBuilder.CreateIndex(
                name: "IX_AntPatologicos_PacienteId",
                table: "AntPatologicos",
                column: "PacienteId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AntNOPatologicos_PacienteId",
                table: "AntNOPatologicos",
                column: "PacienteId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AntHeredoFamiliares_PacienteId",
                table: "AntHeredoFamiliares",
                column: "PacienteId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AntGinecoObstetricos_PacienteId",
                table: "AntGinecoObstetricos",
                column: "PacienteId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_AntPatologicos_PacienteId",
                table: "AntPatologicos");

            migrationBuilder.DropIndex(
                name: "IX_AntNOPatologicos_PacienteId",
                table: "AntNOPatologicos");

            migrationBuilder.DropIndex(
                name: "IX_AntHeredoFamiliares_PacienteId",
                table: "AntHeredoFamiliares");

            migrationBuilder.DropIndex(
                name: "IX_AntGinecoObstetricos_PacienteId",
                table: "AntGinecoObstetricos");

            migrationBuilder.CreateIndex(
                name: "IX_AntPatologicos_PacienteId",
                table: "AntPatologicos",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_AntNOPatologicos_PacienteId",
                table: "AntNOPatologicos",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_AntHeredoFamiliares_PacienteId",
                table: "AntHeredoFamiliares",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_AntGinecoObstetricos_PacienteId",
                table: "AntGinecoObstetricos",
                column: "PacienteId");
        }
    }
}
