﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using consul_express_api.Models.DB;

namespace consul_express_api.Migrations
{
    [DbContext(typeof(ConsultorioDBContext))]
    [Migration("20220202212136_removing-required-apellidoM")]
    partial class removingrequiredapellidoM
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.21")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("consul_express_api.Models.DB.AntGinecoObstetricos", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<short>("Abortos")
                        .HasColumnType("smallint");

                    b.Property<short>("Cesareas")
                        .HasColumnType("smallint");

                    b.Property<bool>("Dismenorreica")
                        .HasColumnType("bit");

                    b.Property<string>("Doc")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("Eumenorreica")
                        .HasColumnType("bit");

                    b.Property<string>("FechaPrimerParto")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Fpp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Fua")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Fuc")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Fum")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Fup")
                        .HasColumnType("nvarchar(max)");

                    b.Property<short>("Gesta")
                        .HasColumnType("smallint");

                    b.Property<short>("IVSA")
                        .HasColumnType("smallint");

                    b.Property<short>("Monarca")
                        .HasColumnType("smallint");

                    b.Property<string>("Mpf")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<Guid>("PacienteId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<short>("Para")
                        .HasColumnType("smallint");

                    b.Property<string>("Ritmo")
                        .HasColumnType("nvarchar(15)")
                        .HasMaxLength(15);

                    b.HasKey("Id");

                    b.HasIndex("PacienteId")
                        .IsUnique();

                    b.ToTable("AntGinecoObstetricos");
                });

            modelBuilder.Entity("consul_express_api.Models.DB.AntHeredoFamiliares", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Asma")
                        .HasColumnType("nvarchar(30)")
                        .HasMaxLength(30);

                    b.Property<string>("Cancer")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("EnfCronicoDegenerativas")
                        .HasColumnType("nvarchar(1000)")
                        .HasMaxLength(1000);

                    b.Property<Guid>("PacienteId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Tuberculosis")
                        .HasColumnType("nvarchar(30)")
                        .HasMaxLength(30);

                    b.HasKey("Id");

                    b.HasIndex("PacienteId")
                        .IsUnique();

                    b.ToTable("AntHeredoFamiliares");
                });

            modelBuilder.Entity("consul_express_api.Models.DB.AntNOPatologicos", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("EstadoCivil")
                        .HasColumnType("nvarchar(20)")
                        .HasMaxLength(20);

                    b.Property<string>("HabitosHigieneDieta")
                        .HasColumnType("nvarchar(10)")
                        .HasMaxLength(10);

                    b.Property<string>("Ocupacion")
                        .HasColumnType("nvarchar(20)")
                        .HasMaxLength(20);

                    b.Property<Guid>("PacienteId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("PacienteId")
                        .IsUnique();

                    b.ToTable("AntNOPatologicos");
                });

            modelBuilder.Entity("consul_express_api.Models.DB.AntPatologicos", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Alcoholismo")
                        .HasColumnType("nvarchar(15)")
                        .HasMaxLength(15);

                    b.Property<string>("Alergias")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Cirugias")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Enfermedades")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Fracturas")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Fumador")
                        .HasColumnType("nvarchar(100)")
                        .HasMaxLength(100);

                    b.Property<string>("GrupoRH")
                        .HasColumnType("nvarchar(15)")
                        .HasMaxLength(15);

                    b.Property<Guid>("PacienteId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Transfusiones")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("PacienteId")
                        .IsUnique();

                    b.ToTable("AntPatologicos");
                });

            modelBuilder.Entity("consul_express_api.Models.DB.Historial", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("AnalisisIDX")
                        .HasColumnType("nvarchar(max)")
                        .HasMaxLength(10000);

                    b.Property<string>("LaboratorioYGabinete")
                        .HasColumnType("nvarchar(max)")
                        .HasMaxLength(10000);

                    b.Property<string>("Objetivo")
                        .HasColumnType("nvarchar(max)")
                        .HasMaxLength(10000);

                    b.Property<Guid>("PacienteId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Plan")
                        .HasColumnType("nvarchar(max)")
                        .HasMaxLength(10000);

                    b.Property<string>("Subjetivo")
                        .HasColumnType("nvarchar(max)")
                        .HasMaxLength(10000);

                    b.HasKey("Id");

                    b.HasIndex("PacienteId")
                        .IsUnique();

                    b.ToTable("Historial");
                });

            modelBuilder.Entity("consul_express_api.Models.DB.Paciente", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("ApellidoM")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ApellidoP")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Direccion")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<short>("Edad")
                        .HasColumnType("smallint");

                    b.Property<DateTime>("FechaNacimiento")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("FechaRegistro")
                        .HasColumnType("datetime2");

                    b.Property<bool>("Lotus")
                        .HasColumnType("bit");

                    b.Property<string>("MotivoConsulta")
                        .HasColumnType("nvarchar(1000)")
                        .HasMaxLength(1000);

                    b.Property<string>("Nombre")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Telefono")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Pacientes");
                });

            modelBuilder.Entity("consul_express_api.Models.DB.AntGinecoObstetricos", b =>
                {
                    b.HasOne("consul_express_api.Models.DB.Paciente", null)
                        .WithOne("AntGinecoObstetricos")
                        .HasForeignKey("consul_express_api.Models.DB.AntGinecoObstetricos", "PacienteId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("consul_express_api.Models.DB.AntHeredoFamiliares", b =>
                {
                    b.HasOne("consul_express_api.Models.DB.Paciente", null)
                        .WithOne("AntHeredoFamiliares")
                        .HasForeignKey("consul_express_api.Models.DB.AntHeredoFamiliares", "PacienteId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("consul_express_api.Models.DB.AntNOPatologicos", b =>
                {
                    b.HasOne("consul_express_api.Models.DB.Paciente", null)
                        .WithOne("AntNOPatologicos")
                        .HasForeignKey("consul_express_api.Models.DB.AntNOPatologicos", "PacienteId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("consul_express_api.Models.DB.AntPatologicos", b =>
                {
                    b.HasOne("consul_express_api.Models.DB.Paciente", null)
                        .WithOne("AntPatologicos")
                        .HasForeignKey("consul_express_api.Models.DB.AntPatologicos", "PacienteId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("consul_express_api.Models.DB.Historial", b =>
                {
                    b.HasOne("consul_express_api.Models.DB.Paciente", null)
                        .WithOne("Historial")
                        .HasForeignKey("consul_express_api.Models.DB.Historial", "PacienteId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
