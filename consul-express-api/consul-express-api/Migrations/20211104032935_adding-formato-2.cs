﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace consul_express_api.Migrations
{
    public partial class addingformato2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Historial",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Subjetivo = table.Column<string>(maxLength: 10000, nullable: true),
                    LaboratorioYGabinete = table.Column<string>(maxLength: 10000, nullable: true),
                    Objetivo = table.Column<string>(maxLength: 10000, nullable: true),
                    AnalisisIDX = table.Column<string>(maxLength: 10000, nullable: true),
                    Plan = table.Column<string>(maxLength: 10000, nullable: true),
                    PacienteId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Historial", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Historial_Pacientes_PacienteId",
                        column: x => x.PacienteId,
                        principalTable: "Pacientes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Historial_PacienteId",
                table: "Historial",
                column: "PacienteId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Historial");
        }
    }
}
