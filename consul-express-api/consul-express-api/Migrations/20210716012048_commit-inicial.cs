﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace consul_express_api.Migrations
{
    public partial class commitinicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Pacientes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Nombre = table.Column<string>(nullable: false),
                    ApellidoP = table.Column<string>(nullable: false),
                    ApellidoM = table.Column<string>(nullable: false),
                    Direccion = table.Column<string>(nullable: false),
                    Edad = table.Column<short>(nullable: false),
                    Telefono = table.Column<string>(nullable: false),
                    MotivoConsulta = table.Column<string>(maxLength: 1000, nullable: true),
                    FechaNacimiento = table.Column<DateTime>(nullable: false),
                    FechaRegistro = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pacientes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AntGinecoObstetricos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Monarca = table.Column<short>(nullable: false),
                    Ritmo = table.Column<string>(maxLength: 15, nullable: true),
                    Eumenorreica = table.Column<bool>(nullable: false),
                    Dismenorreica = table.Column<bool>(nullable: false),
                    IVSA = table.Column<short>(nullable: false),
                    Gesta = table.Column<short>(nullable: false),
                    Para = table.Column<short>(nullable: false),
                    Abortos = table.Column<short>(nullable: false),
                    Cesareas = table.Column<short>(nullable: false),
                    Fup = table.Column<string>(nullable: true),
                    Fuc = table.Column<string>(nullable: true),
                    Fua = table.Column<string>(nullable: true),
                    FechaPrimerParto = table.Column<string>(nullable: true),
                    Mpf = table.Column<string>(maxLength: 50, nullable: true),
                    Doc = table.Column<string>(nullable: true),
                    Fum = table.Column<string>(nullable: true),
                    Fpp = table.Column<string>(nullable: true),
                    PacienteId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AntGinecoObstetricos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AntGinecoObstetricos_Pacientes_PacienteId",
                        column: x => x.PacienteId,
                        principalTable: "Pacientes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AntHeredoFamiliares",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Cancer = table.Column<string>(nullable: true),
                    Tuberculosis = table.Column<string>(maxLength: 30, nullable: true),
                    Asma = table.Column<string>(maxLength: 30, nullable: true),
                    EnfCronicoDegenerativas = table.Column<string>(maxLength: 1000, nullable: true),
                    PacienteId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AntHeredoFamiliares", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AntHeredoFamiliares_Pacientes_PacienteId",
                        column: x => x.PacienteId,
                        principalTable: "Pacientes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AntNOPatologicos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EstadoCivil = table.Column<string>(maxLength: 20, nullable: true),
                    Ocupacion = table.Column<string>(maxLength: 20, nullable: true),
                    HabitosHigieneDieta = table.Column<string>(maxLength: 10, nullable: true),
                    PacienteId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AntNOPatologicos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AntNOPatologicos_Pacientes_PacienteId",
                        column: x => x.PacienteId,
                        principalTable: "Pacientes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AntPatologicos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Fumador = table.Column<string>(maxLength: 100, nullable: true),
                    Alcoholismo = table.Column<string>(maxLength: 15, nullable: true),
                    Alergias = table.Column<string>(nullable: true),
                    Fracturas = table.Column<string>(nullable: true),
                    Transfusiones = table.Column<string>(nullable: true),
                    Cirugias = table.Column<string>(nullable: true),
                    Enfermedades = table.Column<string>(nullable: true),
                    GrupoRH = table.Column<string>(maxLength: 15, nullable: true),
                    PacienteId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AntPatologicos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AntPatologicos_Pacientes_PacienteId",
                        column: x => x.PacienteId,
                        principalTable: "Pacientes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AntGinecoObstetricos_PacienteId",
                table: "AntGinecoObstetricos",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_AntHeredoFamiliares_PacienteId",
                table: "AntHeredoFamiliares",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_AntNOPatologicos_PacienteId",
                table: "AntNOPatologicos",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_AntPatologicos_PacienteId",
                table: "AntPatologicos",
                column: "PacienteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AntGinecoObstetricos");

            migrationBuilder.DropTable(
                name: "AntHeredoFamiliares");

            migrationBuilder.DropTable(
                name: "AntNOPatologicos");

            migrationBuilder.DropTable(
                name: "AntPatologicos");

            migrationBuilder.DropTable(
                name: "Pacientes");
        }
    }
}
