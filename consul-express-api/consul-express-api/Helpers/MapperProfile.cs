﻿using AutoMapper;
using consul_express_api.Models.DB;
using consul_express_api.Models.Schemas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Helpers
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Paciente, PacienteSchema>();
            CreateMap<PacienteSchema, Paciente>();

            CreateMap<RespaldoHistorial, RespaldoSchema>();
            CreateMap<RespaldoSchema, RespaldoHistorial>();
        }
    }
}
