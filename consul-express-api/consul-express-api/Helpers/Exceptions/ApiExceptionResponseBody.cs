﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consul_express_api.Helpers.Exceptions
{
    public class ApiExceptionResponseBody
    {
        // ReSharper disable once InconsistentNaming
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]  // Causes json serialization
        private readonly ApiException Exception;

        public ApiExceptionResponseBody(Exception exception)
        {
            Exception = new ApiException(exception);
        }

        public ApiExceptionResponseBody(string message)
        {
            Exception = new ApiException(message);
        }

        public override string ToString()
        {
            if (Exception == null) return "";

            var b = new StringBuilder();
            if (!string.IsNullOrEmpty(Exception.Source))
            {
                b.Append("Source: ");
                b.Append(Exception.Source);
            }
            if (!string.IsNullOrEmpty(Exception.Reason))
            {
                if (b.Length > 0)
                    b.Append(" | ");
                b.Append("Reason: ");
                b.Append(Exception.Reason);
            }
            if (!string.IsNullOrEmpty(Exception.Detail))
            {
                if (b.Length > 0)
                    b.Append(" | ");
                b.Append("Detail: ");
                b.Append(Exception.Detail);
            }
            if (!string.IsNullOrEmpty(Exception.MessageId))
            {
                if (b.Length > 0)
                    b.Append(" | ");
                b.Append("MessageId: ");
                b.Append(Exception.MessageId);
            }

            return b.ToString();
        }

        public string GetMessageId()
        {
            return Exception?.MessageId;
        }

        public string GetSource()
        {
            return Exception?.Source;
        }

        public string GetReason()
        {
            return Exception?.Reason;
        }

        public string GetDetail()
        {
            return Exception?.Detail;
        }

        // This class is private and only accessible within ApiExceptionResponseBody because 
        // ApiExceptionResponseBody is the only valid format for the API to return an error 
        private class ApiException
        {
            public string MessageId { get; }
            public string Detail { get; }
            public string Reason { get; }
            public string Source { get; }

            private ApiException(string messageId, string reason, string detail, string source)
            {
                MessageId = messageId;
                Reason = reason;
                Detail = detail;
                Source = source;
            }

            public ApiException(string reason, string detail = "", string source = "")
                : this(Guid.NewGuid().ToString(), reason, detail, source)
            {
            }

            public ApiException(Exception exception)
                : this(GetExceptionMessage(exception), "", exception?.Source)
            {
            }

            private static string GetExceptionMessage(Exception exception)
            {
                var sb = new StringBuilder(exception?.Message);
                var curException = exception;
                while (curException?.InnerException != null)
                {
                    curException = curException.InnerException;
                    sb.Append($" | {curException.Message}");
                }

                return sb.ToString();
            }
        }

    }
}
