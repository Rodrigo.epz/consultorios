﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Helpers.Exceptions
{
    public class DuplicateEntityException : Exception
    {
        public DuplicateEntityException(string message)
          : base(message)
        {
        }
    }
}
