﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Helpers
{
    public static class Constantes
    {
        public const string XNoPuedeSerY = "El {0} debe diferente a {1}";
        public const string XDebeSerMayor0 = "El {0} debe ser mayor a 0";
        public const string EntidadXYaExiste = "El {0} ya existe";
        public const string EntidadXConIdNoExiste = "El {0} con el id {1} no existe";

    }
}
