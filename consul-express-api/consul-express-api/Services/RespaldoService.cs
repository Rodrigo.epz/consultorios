﻿using AutoMapper;
using consul_express_api.Models.DB;
using consul_express_api.Models.Schemas;
using consul_express_api.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;

namespace consul_express_api.Services
{
    public class RespaldoService : IRespaldoService
    {
        private ConsultorioDBContext _context;
        private IMapper _mapper;

        public RespaldoService(ConsultorioDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<RespaldoSchema> Save(RespaldoSchema respaldo)
        {
            var respaldoDB = _context.RespaldoHistorial.FirstOrDefault(r => r.NombreArchivo == respaldo.NombreArchivo);
            if(respaldoDB == null)
            {
                respaldoDB = new RespaldoHistorial();
                respaldoDB.FechaCreacion = DateTime.Now;
                respaldoDB.NombreArchivo = respaldo.NombreArchivo;
                respaldoDB.CantidadPacientesRespaldados = respaldo.CantidadPacientesRespaldados;
                _context.Add(respaldoDB);
                await _context.SaveChangesAsync();
                return respaldo;
            }
            else
            {
                respaldoDB.CantidadPacientesRespaldados = respaldo.CantidadPacientesRespaldados;
                _context.Update(respaldoDB);
                await _context.SaveChangesAsync();
                return _mapper.Map<RespaldoSchema>(respaldoDB);
            }
        }

        public async Task<List<RespaldoSchema>> GetAll()
        {
            return _mapper.Map<List<RespaldoSchema>>(_context.RespaldoHistorial.OrderByDescending(r => r.FechaCreacion));
        }
    }
}
    