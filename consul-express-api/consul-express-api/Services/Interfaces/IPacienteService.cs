﻿using consul_express_api.Models.Schemas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Services.Interfaces
{
    public interface IPacienteService
    {
        Task<PacienteSchema> Get(Guid id); 
        Task<PacientesPaginado> GetAll(ParametrosBusqueda parametros); 
        Task<Guid> Add(PacienteSchema paciente);
        Task<PacienteSchema> Update(PacienteSchema paciente);
        Task<PacienteSchema> Delete(Guid id, bool borradoLogico); 
    }
}
