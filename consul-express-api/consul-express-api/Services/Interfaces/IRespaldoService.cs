﻿using consul_express_api.Models.Schemas;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace consul_express_api.Services.Interfaces
{
    public interface IRespaldoService
    {
        Task<RespaldoSchema> Save(RespaldoSchema respaldo);
        Task<List<RespaldoSchema>> GetAll();
    }
}
