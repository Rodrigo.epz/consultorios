﻿using AutoMapper;
using consul_express_api.Helpers;
using consul_express_api.Helpers.Exceptions;
using consul_express_api.Helpers.Extensions;
using consul_express_api.Models.DB;
using consul_express_api.Models.Schemas;
using consul_express_api.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace consul_express_api.Services
{
    public class PacienteService : IPacienteService
    {
        private ConsultorioDBContext _context;
        private readonly IMapper _mapper;
        public PacienteService(ConsultorioDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<PacienteSchema> Get(Guid id)
        {
            var paciente = await _context.Pacientes
                .Include(p => p.AntPatologicos)
                .Include(p => p.AntNOPatologicos)
                .Include(p => p.AntHeredoFamiliares)
                .Include(p => p.AntGinecoObstetricos)
                .Include(p => p.Historial)
                .FirstOrDefaultAsync(p => p.Id == id);
            if(paciente == null)
            {
                throw new EntityNotFoundException(String.Format("No existe el {0} con el id = {1}", nameof(paciente), id));
            }
            return _mapper.Map<PacienteSchema>(paciente);
        }
        public async Task<PacientesPaginado> GetAll(ParametrosBusqueda parametros)
        {
            var pacientes = BuscaPacientesPorParametros(parametros);

            var pacientesLista = _mapper.Map<IEnumerable<PacienteSchema>>(pacientes.Item1);
            int cantidadPacientes = pacientes.Item2;

            return new PacientesPaginado(pacientesLista, cantidadPacientes, parametros.itemsPorPagina);
        }
        public async Task<Guid> Add(PacienteSchema paciente)
        {
            if (PacienteExists(paciente))
            {
                throw new DuplicateEntityException(string.Format(Constantes.EntidadXYaExiste, nameof(paciente)));
            }
            var pacienteDTO = _mapper.Map<Paciente>(paciente);
            pacienteDTO.FechaRegistro = DateTime.UtcNow;
            _context.Pacientes.Add(pacienteDTO);
            await _context.SaveChangesAsync();
            return pacienteDTO.Id;
        }
        public async Task<PacienteSchema> Update(PacienteSchema paciente)
        {
            var pacienteDTO = await _context.Pacientes.FirstOrDefaultAsync(p => p.Id == paciente.Id);

            if(pacienteDTO == null)
            {
                throw new EntityNotFoundException(string.Format(Constantes.EntidadXConIdNoExiste, nameof(paciente), paciente.Id));
            }
            pacienteDTO.AntPatologicos = null;
            pacienteDTO.AntNOPatologicos = null;
            pacienteDTO.AntHeredoFamiliares = null;
            pacienteDTO.AntGinecoObstetricos = null;
            pacienteDTO.Edad = paciente.Edad;
            pacienteDTO.Nombre = paciente.Nombre;
            pacienteDTO.ApellidoP = paciente.ApellidoP;
            pacienteDTO.ApellidoM = paciente.ApellidoM;
            pacienteDTO.Direccion = paciente.Direccion;
            pacienteDTO.FechaNacimiento = paciente.FechaNacimiento;
            pacienteDTO.Telefono = paciente.Telefono;
            pacienteDTO.MotivoConsulta= paciente.MotivoConsulta;
            pacienteDTO.Edad = paciente.Edad;
            pacienteDTO.Lotus= paciente.Lotus;

            _context.Entry(pacienteDTO).State = EntityState.Modified;
           
            await _context.SaveChangesAsync();
            return await Get(pacienteDTO.Id);
        }
        public async Task<PacienteSchema> Delete(Guid id, bool borradoLogico)
        {
            var paciente = await _context.Pacientes.FirstOrDefaultAsync(p => p.Id == id);
            if (paciente == null)
                throw new EntityNotFoundException(string.Format(Constantes.EntidadXConIdNoExiste, nameof(paciente), id));

            _context.Pacientes.Remove(paciente);
            await _context.SaveChangesAsync();

            return _mapper.Map<PacienteSchema>(paciente);
        }

        private bool PacienteExists(PacienteSchema paciente)
        {
            return _context.Pacientes.Any(e => e.Nombre == paciente.Nombre
            && e.ApellidoM == paciente.ApellidoM
            && e.ApellidoP == paciente.ApellidoP
            && e.FechaNacimiento == paciente.FechaNacimiento);
        }

        private Tuple<IEnumerable<Paciente>,int> BuscaPacientesPorParametros(ParametrosBusqueda parametros)
        {
            var query = _context.Pacientes
                .Include(p => p.AntPatologicos)
                .AsQueryable();

            if (!string.IsNullOrEmpty(parametros.general))
            {
                query = query.Where(p => p.Nombre.Contains(parametros.general) 
                || p.ApellidoP.Contains(parametros.general)
                || p.ApellidoM.Contains(parametros.general)
                || p.MotivoConsulta.Contains(parametros.general));
            }
            else
            {
                if (!string.IsNullOrEmpty(parametros.nombre))
                {
                    query = query.Where(p => EF.Functions.Like(EF.Functions.Collate(p.Nombre, "SQL_LATIN1_GENERAL_CP1_CI_AI"), $"%{parametros.nombre}%"));
                }
                if (!string.IsNullOrEmpty(parametros.apellidoMaterno))
                {
                    query = query.Where(p => EF.Functions.Like(EF.Functions.Collate(p.ApellidoM, "SQL_LATIN1_GENERAL_CP1_CI_AI"), $"%{parametros.apellidoMaterno}%"));
                }
                if (!string.IsNullOrEmpty(parametros.apellidoPaterno))
                {
                    query = query.Where(p => EF.Functions.Like(EF.Functions.Collate(p.ApellidoP, "SQL_LATIN1_GENERAL_CP1_CI_AI"), $"%{parametros.apellidoPaterno}%"));
                }
                if (!string.IsNullOrEmpty(parametros.motivo))
                {
                    query = query.Where(p => p.MotivoConsulta.Contains(parametros.motivo));
                }
            }

            if (!string.IsNullOrEmpty(parametros.ordenaPor) && !string.IsNullOrEmpty(parametros.ordenaDireccion))
            {
                query = parametros.ordenaDireccion == "ASC" ? query.OrderByPropertyName(parametros.ordenaPor) : query.OrderByPropertyNameDescending(parametros.ordenaPor);
            }
            else
            {
                query = query.OrderBy(p => p.Nombre);
            }
            
            int rouwCount = query.Count();
            return new Tuple<IEnumerable<Paciente>, int>
                (query.Skip(parametros.pagina * parametros.itemsPorPagina).Take(parametros.itemsPorPagina),  
                rouwCount);
        }
    }
}
