ALTER PROCEDURE GetAllPacientesInfo
as
SELECT 
			 pac.Id as Id
      ,[Nombre]
      ,[ApellidoP]
      ,[ApellidoM]
      ,[Direccion]
      ,[Edad]
      ,[Telefono]
      ,[MotivoConsulta]
      ,[FechaNacimiento]
      ,[FechaRegistro]
      ,[Lotus]
			,histo.Id as HistoId
      ,[Subjetivo]
      ,[LaboratorioYGabinete]
      ,[Objetivo]
      ,[AnalisisIDX]
      ,[Plan]
			,pat.Id as AntPatId
      ,[Fumador]
      ,[Alcoholismo]
      ,[Alergias]
      ,[Fracturas]
      ,[Transfusiones]
      ,[Cirugias]
      ,[Enfermedades]
      ,[GrupoRH]
			,noPat.Id as AntNoPatId
      ,[EstadoCivil]
      ,[Ocupacion]
      ,[HabitosHigieneDieta]
			,hered.Id as HeredId
      ,[Cancer]
      ,[Tuberculosis]
      ,[Asma]
      ,[EnfCronicoDegenerativas]
			,gine.Id as GineId
      ,[Monarca]
      ,[Ritmo]
      ,[Eumenorreica]
      ,[Dismenorreica]
      ,[IVSA]
      ,[Gesta]
      ,[Para]
      ,[Abortos]
      ,[Cesareas]
      ,[Fup]
      ,[Fuc]
      ,[Fua]
      ,[FechaPrimerParto]
      ,[Mpf]
      ,[Doc]
      ,[Fum]
      ,[Fpp]

FROM Pacientes pac
LEFT JOIN AntPatologicos pat ON pat.PacienteId = pac.Id
LEFT JOIN AntNOPatologicos noPat ON noPat.PacienteId = pac.Id
LEFT JOIN AntHeredoFamiliares hered ON hered.PacienteId = pac.Id
LEFT JOIN AntGinecoObstetricos gine ON gine.PacienteId = pac.Id
LEFT JOIN Historial histo ON histo.PacienteId = pac.Id


SELECT * FROM Pacientes pac
LEFT JOIN AntPatologicos pat ON pat.PacienteId = pac.Id
LEFT JOIN AntNOPatologicos noPat ON noPat.PacienteId = pac.Id
LEFT JOIN AntHeredoFamiliares hered ON hered.PacienteId = pac.Id
LEFT JOIN AntGinecoObstetricos gine ON gine.PacienteId = pac.Id
LEFT JOIN Historial histo ON histo.PacienteId = pac.Id